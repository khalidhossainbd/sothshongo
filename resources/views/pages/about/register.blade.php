@extends('layouts.mainlayout')

@section("title", "Member Registation")

@section('content')

	<section>
		<div class="top-bg-one">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<p class="top-head">Join With Us</p>
					</div>
				</div>
				
			</div>
		</div>
	</section>

	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-6" style="padding: 30px;">
					<div class="row">
						@if(Session::has('message'))
						<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
						@endif
					</div>
					<div class="row">
						@if ($errors->any())
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
					</div>
					<div class="row">
						<p class="member-head">Registration Form</p>
						<hr class="member-hr">
					</div>
					<div class="row">
						{{-- <form method="post" id="free-call-reg"> --}}
							<form method="POST" action="{{ url('/member_register') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
							    {{ csrf_field() }}
							<div class="form-group">
							    <label for="name">Name</label>
							    <input type="text" class="form-control" placeholder="Name" name="name" required="required">
							</div>
							<div class="form-group">
							    <label for="name">Father Name</label>
							    <input type="text" class="form-control" placeholder="Father Name" name="f_name"  required="required">
							</div>
							<div class="form-group">
							    <label for="name">Mother Name</label>
							    <input type="text" class="form-control" placeholder="Mother Name" name="m_name"  required="required">
							</div>
							<div class="form-group">
							    <label for="email">Email </label>
							    <input type="email" class="form-control" placeholder="Email" name="email"  required="required">
							</div>
							<div class="form-group">
							    <label for="mobile">Mobile Number</label>
							    <input type="text" class="form-control" placeholder="Mobile Number" name="mobile"  required="required">
							</div>
							<div class="form-group">
							    <label for="name">Date of Birth</label>
							    <input type="date" class="form-control" placeholder="Birth Date" name="date"  required="required">
							</div>
							<div class="form-group">
							    <label for="name">Occupation</label>
							    <input type="text" class="form-control" placeholder="Occupation" name="occupation" required="required">
							</div>
							<div class="form-group">
							    <label for="name">NID Number</label>
							    <input type="Number" class="form-control" placeholder="National Id" name="nid" required="required">
							</div>

							<div class="form-group">
							    <label for="message">Address</label>
							    <textarea class="form-control" rows="5" name="address"></textarea>
							</div>
							<div class="form-group">
							    <label for="name">Nominee Name</label>
							    <input type="text" class="form-control" placeholder="Nominee Name" name="n_name"  required="required">
							</div>
							<div class="form-group">
							    <label for="name">Relation With Nominee</label>
							    <input type="text" class="form-control" placeholder="Nominee Relationship" name="n_relation" required="required">
							</div>
							<div class="form-group">
							    <label for="name">Nationality</label>
							    <input type="text" class="form-control" id="name" placeholder="Nationality" name="nationality" required="required">
							</div>
						 	<div class="form-group">
						    	<label for="exampleInputFile">File input</label>
						    	<input name="myimage" type="file" id="exampleInputFile">
						    	<p class="help-block">Please Input Passport size Image</p>
							</div>
 											
	 						<div class="checkbox">
							    <label>
							      <input name="conditions" type="checkbox" required="required"> Agree All <a href=""> Terms & Conditions</a>
							    </label>
							</div>

							<button type="submit" class="btn btn-primary btn-block">Submit</button>
						</form>
					</div>
				</div>

				<div class="col-md-6" style="padding: 30px;">
					<div class="row">
						<p class="member-head">Become a member</p>
						<hr class="member-hr">
					</div>
					<div class="row">
						<p class="member-text">
						Any Adult Bangladeshi National can be a member of SHONDHI.
						Required documents are as follows:<br><br>
						<strong>Application Form (Completely filled up by the applicant)<br>
						Passport Sized Photo -3 (Three) Copies for applicant<br>
						Passport Sized Photo -1 (One) Copy for Nominee<br>
						Membership fee- 5000/- (Tk. Five Thousand only). One time only.</strong>
						<br><br>
						Anyone can be a member any time by submitting all the above stated documents to SHONDHI Secretariat, situated at <strong> Road# 28, Block# K, House# 6(Level #3), Beside Banani Puja Math, Dhaka-1206.</strong><br><br>
						Applicant can collect the Registration Form from SHONDHI Reception Desk.
						or,<br>
						Applicant can Download the form from www.shondhisomobai.com and fill up properly. Then send all the required documents to SHONDHI Secretariat. After verification and approved by EC, he/she will be notified wither his/her application is approved or declined. SHONDHI EC reserved all the rights to approve or decline the application.<br><br>
						Applicant may process his/her registration through online registration. For this, you must need to have your valid e-Mail ID which will be used as your 'login id' to get member's page.<br><br>
						Make sure your payment is received properly by SHONDHI Accounts Department.<br><br>
						You are always welcome to SHONDHI.

					</p>
					</div>
				</div>
			</div>
		</div>
	</section>

@endsection

@section('jscript')
{{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
{{-- <script type="text/javascript">
	$(function () {    
	  $("#free-call-reg").submit(function (e) {
	    e.preventDefault();
	    var form_data = $(this).serialize(); 
	    $.ajax({
	      type: "POST", 
	      url: "{{ url('/member_register') }}",
	      dataType: "json", 
	      contentType: false,
	      cache: false,
	      data: form_data
	    }).done(function (data) {
	        // console.log(data);
	        $("#free-call-reg")[0].reset();
	        swal("Thanks You!", "We will call you soon !", "success");
	        // alert("Thanks For your Suggestion!!!");
	    }).fail(function (data) {
	        console.log(data);
	    });
	  }); 
	});
</script> --}}

@endsection