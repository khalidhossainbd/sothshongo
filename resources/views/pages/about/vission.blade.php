@extends('layouts.mainlayout')

@section('content')

<section>
	<div class="top-bg-one">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<p class="top-head">About US</p>
				</div>
			</div>
			
		</div>
	</div>
</section>

<section style="margin: 30px 0;">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<img class="img-thumbnail" src="{{ asset('dist/images/missio-2.jpg') }}" style="margin:30px 0;">
			</div>
			<div class="col-md-7">
				<h2 class="about-head">OUR MISSION</h2>
				<hr class="title-hr">
				<p class="about-text">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto ad aliquam sint odio earum, deserunt debitis nulla voluptatum fuga commodi, id excepturi perferendis? Soluta eaque quas, ut doloribus laborum nam.
				</p>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			
			<div class="col-md-7">
				<h2 class="about-head">OUR VISSION</h2>
				<hr class="title-hr">
				<p class="about-text">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto ad aliquam sint odio earum, deserunt debitis nulla voluptatum fuga commodi, id excepturi perferendis? Soluta eaque quas, ut doloribus laborum nam.  
				</p>
			</div>
			<div class="col-md-5">
				<img class="img-thumbnail" src="{{ asset('dist/images/missio.jpg') }}" style="margin:30px 0;">
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<img class="img-thumbnail" src="{{ asset('dist/images/new-3.jpg') }}" style="margin:30px 0;">
			</div>
			
			<div class="col-md-7">
				<h2 class="about-head">OUR VALUES</h2>
				<hr class="title-hr">
				<p class="about-text">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto ad aliquam sint odio earum, deserunt debitis nulla voluptatum fuga commodi, id excepturi perferendis? Soluta eaque quas, ut doloribus laborum nam. 
				</p>
			</div>
			
		</div>
	</div>
</section>

@endsection