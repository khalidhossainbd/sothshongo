@extends('layouts.mainlayout')

@section('content')

<section>
    <div class="top-bg-two">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <p class="top-head">General Members</p>

                </div>
            </div>

        </div>
    </div>
</section>

<section class="team">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <h2 class="about-head">General Members</h2>
                <hr class="title-hr">
                <div class="row pt-md">
                    @foreach($mambers as $item)
                      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                          <div class="img-box">
                              <img src="{{ asset('uploads/members/'.$item->mem_image) }}" class="img-responsive" alt="general Member">
                              <ul class="text-center">
                                  <a href="#">
                                      <li><i class="fa fa-facebook"></i></li>
                                  </a>
                                  <a href="#">
                                      <li><i class="fa fa-twitter"></i></li>
                                  </a>
                                  <a href="#">
                                      <li><i class="fa fa-linkedin"></i></li>
                                  </a>
                              </ul>
                          </div>
                          <h1> {{ $item->name }} </h1>
                          <h2>General Member</h2>
                      </div>
                    @endforeach
                </div>
                {{-- <div class="row pt-md">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                      <div class="img-box">
                        <img src="{{ asset('dist/images/members/razeeb.jpg') }}" class="img-responsive" alt="founders">
                        <ul class="text-center">
                          <a href="#"><li><i class="fa fa-facebook"></i></li></a>
                          <a href="#"><li><i class="fa fa-twitter"></i></li></a>
                          <a href="#"><li><i class="fa fa-linkedin"></i></li></a>
                        </ul>
                      </div>
                      <h1>Razeeb Hassan</h1>
                      <h2>General Member</h2>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                          <div class="img-box">
                            <img src="{{ asset('dist/images/members/khalid.jpg') }}" class="img-responsive" alt="founders">
                            <ul class="text-center">
                              <a href="#"><li><i class="fa fa-facebook"></i></li></a>
                              <a href="#"><li><i class="fa fa-twitter"></i></li></a>
                              <a href="#"><li><i class="fa fa-linkedin"></i></li></a>
                            </ul>
                          </div>
                          <h1>Md Khalid Hossain</h1>
                          <h2>General Member</h2>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                    <div class="img-box">
                        <img src="{{ asset('dist/images/members/mahamud.jpg') }}" class="img-responsive" alt="founders">
                        <ul class="text-center">
                          <a href="#"><li><i class="fa fa-facebook"></i></li></a>
                          <a href="#"><li><i class="fa fa-twitter"></i></li></a>
                          <a href="#"><li><i class="fa fa-linkedin"></i></li></a>
                        </ul>
                      </div>
                      <h1>Khondaker Mahmud Alam</h1>
                      <h2>General Member</h2>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                      <div class="img-box">
                        <img src="{{ asset('dist/images/members/mostafiz.jpg') }}" class="img-responsive" alt="founders">
                        <ul class="text-center">
                          <a href="#"><li><i class="fa fa-facebook"></i></li></a>
                          <a href="#"><li><i class="fa fa-twitter"></i></li></a>
                          <a href="#"><li><i class="fa fa-linkedin"></i></li></a>
                        </ul>
                      </div>
                      <h1>Md. Mostafizur Rahman</h1>
                      <h2>General Member</h2>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
</section>

@endsection