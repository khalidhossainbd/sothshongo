@extends('layouts.mainlayout')

@section('content')

<section>
	<div class="top-bg-two">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<p class="top-head">EC Committee</p>
				</div>
			</div>
			
		</div>
	</div>
</section>
<section>
	<div class="container">
		<h2 class="about-head">EC Members</h2>
		<hr>
	</div>
</section>
<section style="margin: 30px 0;">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 profile">
			  <div class="img-box">
			    <img src="{{ asset('dist/images/members/razeeb.jpg') }}" class="img-responsive" alt="ex_member">
			    <ul class="text-center">
			      <a href="#"><li><i class="fa fa-facebook"></i></li></a>
			      <a href="#"><li><i class="fa fa-twitter"></i></li></a>
			      <a href="#"><li><i class="fa fa-linkedin"></i></li></a>
			    </ul>
			  </div>
			  <h2 class="about-head" style="text-align: center; font-size: 24px; font-weight: bold;">Razeeb Hassan</h2>
			  <p class="about-text" style=" font-weight: normal; text-align: center;">President</p>
			</div>
			<!-- <div class="col-md-5">
				<img class="img-thumbnail" src="dist/images/ff.jpg" style="margin:30px 0;">
			</div> -->
			<div class="col-md-8">
				<div style="border: 1px solid gray; padding: 0px 20px;">
					<h2 class="message-head">সভাপতির কথা</h2>
					<hr class="title-hr">
					<p class="message-text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero commodi, facilis accusamus culpa quo esse iste labore dolorem et dolor id praesentium voluptatibus sequi ratione enim consequatur deserunt recusandae blanditiis.<br> <br>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo impedit soluta officia veritatis laboriosam atque voluptatum cupiditate, dolorum distinctio ex. Possimus iure, praesentium voluptatibus assumenda a blanditiis deleniti facilis porro.
            <br> <br>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo impedit soluta officia veritatis laboriosam atque voluptatum cupiditate, dolorum distinctio ex. Possimus iure, praesentium voluptatibus assumenda a blanditiis deleniti facilis porro.
					</p>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="team">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="col-lg-12">
          
          <div class="row pt-md">
            
            @foreach($mambers as $item)
              <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                  <div class="img-box">
                      <img src="{{ asset('uploads/members/'.$item->mem_image) }}" class="img-responsive" alt="general Member">
                      <ul class="text-center">
                          <a href="#">
                              <li><i class="fa fa-facebook"></i></li>
                          </a>
                          <a href="#">
                              <li><i class="fa fa-twitter"></i></li>
                          </a>
                          <a href="#">
                              <li><i class="fa fa-linkedin"></i></li>
                          </a>
                      </ul>
                  </div>
                  <h1> {{ $item->name }} </h1>
                  <h2>{{ $item->position }}</h2>
                </div>
              @endforeach
              {{-- <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                <div class="img-box">
                  <img src="{{ asset('dist/images/members/khalid.jpg') }}" class="img-responsive" alt="founders">
                  <ul class="text-center">
                    <a href="#"><li><i class="fa fa-facebook"></i></li></a>
                    <a href="#"><li><i class="fa fa-twitter"></i></li></a>
                    <a href="#"><li><i class="fa fa-linkedin"></i></li></a>
                  </ul>
                </div>
                <h1>Md. Khalid Hossain</h1>
                <h2>Sr. Vice President</h2>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                <div class="img-box">
                  <img src="{{ asset('dist/images/members/mahamud.jpg') }}" class="img-responsive" alt="founders">
                  <ul class="text-center">
                    <a href="#"><li><i class="fa fa-facebook"></i></li></a>
                    <a href="#"><li><i class="fa fa-twitter"></i></li></a>
                    <a href="#"><li><i class="fa fa-linkedin"></i></li></a>
                  </ul>
                </div>
                <h1>Khondaker Mahmud Alam</h1>
                <h2>Secretary General</h2>
              </div>
          	  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                <div class="img-box">
                  <img src="{{ asset('dist/images/members/mostafiz.jpg') }}" class="img-responsive" alt="founders">
                  <ul class="text-center">
                    <a href="#"><li><i class="fa fa-facebook"></i></li></a>
                    <a href="#"><li><i class="fa fa-twitter"></i></li></a>
                    <a href="#"><li><i class="fa fa-linkedin"></i></li></a>
                  </ul>
                </div>
                <h1>Md. Mostafizur Rahman</h1>
                <h2>Organizational Secretary and Treasurer</h2>
              </div> --}}


          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection