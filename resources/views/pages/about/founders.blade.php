@extends('layouts.mainlayout')

@section('content')

<section>
	<div class="top-bg-two">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<p class="top-head">Founder Members</p>
				</div>
			</div>
			
		</div>
	</div>
</section>


<section class="team">
  <div class="container">
    <div class="row">
        <div class="col-lg-12">
          	<h2 class="about-head">Founders </h2>
          	<hr>

          	<div class="row">
          		<div class="col-md-6">
          			<div class="row">
          				<div class="col-md-6">
          					<img src="{{ asset('dist/images/members/mahamud.jpg') }}" class="img-responsive" alt="founders">
          				</div>
          				<div class="col-md-6">
          					<h3>Khondaker Mahmud Alam</h3>
          					<h5>Secretary General</h5>
          					<ul class="text-center-icon">
          					  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
          					  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
          					  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
          					</ul>
          				</div>
          			</div>
          		</div>
          		<div class="col-md-6">
          			<div class="row">
          				<div class="col-md-6">
          					<img src="{{ asset('dist/images/members/khalid.jpg') }}" class="img-responsive" alt="founders">
          				</div>
          				<div class="col-md-6">
          					<h3>Md Khalid Hossain</h3>
          					<h5>Sr. Vice President</h5>
          					<ul class="text-center-icon">
          					  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
          					  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
          					  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
          					</ul>
          				</div>
          			</div>
          		</div>
          	</div>
          	
        </div>
    </div>
  </div>
</section>

<section class="team">
  <div class="container">
    <div class="row">
        <div class="col-lg-12">
          	<h2 class="about-head">Co-Founders </h2>
          	<hr>

          	<div class="row pt-md">
          		<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
          		  <div class="img-box">
          		    <img src="{{ asset('dist/images/members/razeeb.jpg') }}" class="img-responsive" alt="founders">
          		    <ul class="text-center">
          		      <a href="#"><li><i class="fa fa-facebook"></i></li></a>
          		      <a href="#"><li><i class="fa fa-twitter"></i></li></a>
          		      <a href="#"><li><i class="fa fa-linkedin"></i></li></a>
          		    </ul>
          		  </div>
          		  <h1>Razeeb Hassan</h1>
          		  <h2>President</h2>
          		</div>

	          	<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
	              <div class="img-box">
	                <img src="{{ asset('dist/images/members/mostafiz.jpg') }}" class="img-responsive" alt="founders">
	                <ul class="text-center">
	                  <a href="#"><li><i class="fa fa-facebook"></i></li></a>
	                  <a href="#"><li><i class="fa fa-twitter"></i></li></a>
	                  <a href="#"><li><i class="fa fa-linkedin"></i></li></a>
	                </ul>
	              </div>
	              <h1>Md. Mostafizur Rahman</h1>
	              <h2>Organizational Secretary and Treasurer</h2>
	            </div>
	            
	            
	            
	            {{-- <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
	              <div class="img-box">
	                <img src="{{ asset('dist/images/members/abdullah.jpg') }}" class="img-responsive" alt="founders">
	                <ul class="text-center">
	                  <a href="#"><li><i class="fa fa-facebook"></i></li></a>
	                  <a href="#"><li><i class="fa fa-twitter"></i></li></a>
	                  <a href="#"><li><i class="fa fa-linkedin"></i></li></a>
	                </ul>
	              </div>
	              <h1>Abdullah-Bin-Mansur</h1>
	              <h2>Asst. Secretary General</h2>
	              
	            </div>
	            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
	              <div class="img-box">
	                <img src="{{ asset('dist/images/members/mamun.jpg') }}" class="img-responsive" alt="founders">
	                <ul class="text-center">
	                  <a href="#"><li><i class="fa fa-facebook"></i></li></a>
	                  <a href="#"><li><i class="fa fa-twitter"></i></li></a>
	                  <a href="#"><li><i class="fa fa-linkedin"></i></li></a>
	                </ul>
	              </div>
	              <h1>Foyjullah Al Mamun </h1>
	              <h2>Asst. Treasurer</h2>
	            </div> --}}
	            
          	</div>
        </div>
    </div>
  </div>
</section>

@endsection