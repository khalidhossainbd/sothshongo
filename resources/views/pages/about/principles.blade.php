@extends('layouts.mainlayout')

@section('content')

	<section>
		<div class="top-bg-six">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<p class="top-head">OUR PRINCIPLES</p>
					</div>
				</div>
				
			</div>
		</div>
	</section>

	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<img src="{{ asset('dist/images/missio-3.jpg') }}" alt="Our Story" class="img-thumbnail" style="margin-top: 40px;">
				</div>
				<div class="col-md-8">
					<p class="our-story-text">
						1. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt dicta quia porro blanditiis neque maxime fuga temporibus, id sint quos. Veniam nesciunt maiores aut excepturi! Nam velit accusantium architecto recusandae. <br><br>
						2. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt dicta quia porro blanditiis neque maxime fuga temporibus, id sint quos. Veniam nesciunt maiores aut excepturi! Nam velit accusantium architecto recusandae. <br><br>
						3. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt dicta quia porro blanditiis neque maxime fuga temporibus, id sint quos. Veniam nesciunt maiores aut excepturi! Nam velit accusantium architecto recusandae.<br><br>
						4. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt dicta quia porro blanditiis neque maxime fuga temporibus, id sint quos. Veniam nesciunt maiores aut excepturi! Nam velit accusantium architecto recusandae.<br><br>
						5. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt dicta quia porro blanditiis neque maxime fuga temporibus, id sint quos. Veniam nesciunt maiores aut excepturi! Nam velit accusantium architecto recusandae.
					</p>
				</div>
			</div>
			
		</div>
	</section>

@endsection