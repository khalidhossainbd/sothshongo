@extends('layouts.mainlayout')

@section('content')

<section>
		<div class="top-bg-five">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<p class="top-head">Top Services</p>
					</div>
				</div>
				
			</div>
		</div>
	</section>

	<section style="margin: 30px 0;">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<img class="img-thumbnail" src="{{ asset('dist/images/services/968618.jpg') }}" style="margin:30px 0;">
				</div>
				<div class="col-md-7">
					<h2 class="about-head">Organic Food</h2>
					<hr class="title-hr">
					<p class="about-text">
						Bengali cuisine is rich in taste and diversity and has a long history too. But with the rapid and radical transformation from thousands of years of farmer based agricultural practice and production to modern industrial system using machines chemicals and pesticides the food we eat today not only raises question about its nutrition value rather threatens us with cause of health hazard. 
						<br><br>
						Therefore, in search of a newer and healthier lifestyle Shondhi plans to go organic adopting a mission “Go green Grow organic”. This means turning to food that is not genetically modified, grown without pesticides, chemicals and antibiotics and has natural connection to the land.
						<br><br>
						Organic food industry is highly regulated. In a challenging environment where terms and conditions are strictly adhered, crops are grown in naturally maintained soil in an environment friendly way, livestock having access to outdoor and organic feed with no antibiotics Shaondhi aims to bring all those to the table of the mass, and at an affordable price.   
					</p>
				</div>
			</div>
		</div>
		
	</section>

@endsection