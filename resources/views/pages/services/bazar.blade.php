@extends('layouts.mainlayout')

@section('content')

<section>
		<div class="top-bg-four">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<p class="top-head">Top Services</p>
					</div>
				</div>
				
			</div>
		</div>
	</section>

	<section style="margin: 30px 0;">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<img class="img-thumbnail" src="{{ asset('dist/images/services/ecommerce.jpg') }}" style="margin:30px 0;">
				</div>
				<div class="col-md-7">
					<h2 class="about-head">Shondhi Bazar</h2>
					<hr class="title-hr">
					<p class="about-text">
						In a world when virtual reality is overtaking the conventional practice, the concept of modern day shopping has now moved a lot to Online shopping than to the In store shopping.
						<br><br>
						Buying and selling of products using internet platform known as e-commerce site has gained world-wide popularity, and Bangladesh is no exception. The rationale behind using e-commerce site or online shop is to take a range of product from many vendors together to the doorstep of a customer at a competitive price saving time money and energy. However the presence of numerous e-commerce sites offering same service comes with a reason where each has its own standard and quality driven by their own philosophy and strategy with variety of terms and conditions attached.
						<br><br>
						Unlike many Shondhi e-commerce site shondhibazar.com comes with a unique commitment. Driven by its philosophy “Go green Grow organic” Shondhi e-commerce site sets its goal primarily selling only Shondhi products and if needed, only of those who meets the ethical standard and quality that Shondhi subscribe to. 

					</p>
				</div>
			</div>
		</div>
		
	</section>

@endsection