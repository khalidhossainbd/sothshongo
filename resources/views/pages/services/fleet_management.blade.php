@extends('layouts.mainlayout')

@section('content')

<section>
		<div class="top-bg-three">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<p class="top-head">Top Services</p>
					</div>
				</div>
				
			</div>
		</div>
	</section>

	<section style="margin: 30px 0;">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<img class="img-thumbnail" src="{{ asset('dist/images/services/fleetbanner-rebuild.png') }}" style="margin:30px 0;">
				</div>
				<div class="col-md-7">
					<h2 class="about-head">Transport fleet management</h2>
					<hr class="title-hr">
					<p class="about-text">
						Transportation in the street of Dhaka has never been easy. Especially for the transport owners it is all the more difficult, and at times a nightmare. Registration, route permit, insurance, fitness, driver selection, driving license, traffic law, maintenance are only a few of an ever ending list what a transport owner needs to handle on a daily basis.
						<br><br>	
						The co-operative philosophy of Shondhi has a solution to this problem. Its strength to create a bridge among the transport owner, maintenance service provider, Govt. agencies and the driver is set to address the problem benefiting everyone spending a little but gaining a lot.
						<br><br>
						Therefore Shondhi invites all transport owners involved in the business of rent-a-car or registered with Uber, Pathao, Shohoj etc. to enlist their vehicle with Shondhi’s Transport Fleet Management team and enjoy the earning while Shondhi strives to deal with all the hassle.
					</p>
				</div>
			</div>
		</div>
	</section>

@endsection