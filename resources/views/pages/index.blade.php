@extends('layouts.mainlayout')

@section('content')

	<section>
		<div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
		    <ol class="carousel-indicators">
		        <li data-target="#carousel" data-slide-to="0" class="active"></li>
		        <li data-target="#carousel" data-slide-to="1"></li>
		        <li data-target="#carousel" data-slide-to="2"></li>
		        <li data-target="#carousel" data-slide-to="3"></li>
		        <li data-target="#carousel" data-slide-to="4"></li>
		    </ol>
		    <!-- Carousel items -->
		    <div class="carousel-inner carousel-zoom">
		        <div class="active item"><img class="img-responsive" src="{{ asset('dist/images/slider/slider-five-new.jpg') }}">
		          <div class="carousel-caption">
		          	<img class="img-responsive" src="{{ asset('dist/images/logo-3.png') }}" style="max-height: 120px; padding:3px; margin: 10px auto;">
		            <h2 class="carousel-head">সমৃদ্ধির যাত্রায় আমরা ক’জনা</h2>
		            <!-- <h2 class="carousel-head">Together for a better tomorrow</h2> -->
		            <!-- <p>Description</p> -->
		          </div>
		        </div>
		        <div class="item"><img class="img-responsive" src="{{ asset('dist/images/slider/slider-two-new.jpg') }}">
		          <div class="carousel-caption">
		          	<img class="img-responsive" src="{{ asset('dist/images/logo-3.png') }}" style="max-height: 120px; padding:3px; margin: 10px auto;">
		          	<h2 class="carousel-head">Together for a better tomorrow</h2> 
		            <!-- <h2 class="carousel-head">Title</h2>
		            <p>Description</p> -->
		          </div>
		        </div>
		        <div class="item"><img class="img-responsive" src="{{ asset('dist/images/slider/slider-one.jpg') }}">
		          <div class="carousel-caption">
		          	<img class="img-responsive" src="{{ asset('dist/images/logo-3.png') }}" style="max-height: 120px; padding:3px; margin: 10px auto;">
		            <h2 class="carousel-head">সমৃদ্ধির যাত্রায় আমরা ক’জনা</h2>
		            <!-- <h2 class="carousel-head">Title</h2>
		            <p>Description</p> -->
		          </div>
		        </div>
		        <div class="item"><img class="img-responsive" src="{{ asset('dist/images/slider/slider-two.jpg') }}">
		          <div class="carousel-caption">
		          	<img class="img-responsive" src="{{ asset('dist/images/logo-3.png') }}" style="max-height: 120px; padding:3px; margin: 10px auto;">
		          	<h2 class="carousel-head">Together for a better tomorrow</h2> 
		            <!-- <h2 class="carousel-head">Go Green Grow Organic...</h2> -->
		            <!-- <p></p> -->
		          </div>
		        </div>
		        <div class="item"><img class="img-responsive" src="{{ asset('dist/images/slider/slider-w-five.jpg') }}">
		          <div class="carousel-caption">
		          	<img class="img-responsive" src="{{ asset('dist/images/logo-3.png') }}" style="max-height: 120px; padding:3px; margin: 10px auto;">
		            <h2 class="carousel-head">সমৃদ্ধির যাত্রায় আমরা ক’জনা</h2>
		            <!-- <h2 class="carousel-head">Title</h2>
		            <p>Description</p> -->
		          </div>
		        </div>
		       </div>
		    <!-- Carousel nav -->
		  <!--   <a class="carousel-control left" href="#carousel" data-slide="prev">‹</a>
		    <a class="carousel-control right" href="#carousel" data-slide="next">›</a> -->
		</div>
		<!-- <div class="container">
			<div style="position: absolute; z-index: 999; top:30%; left:35%;">
				<img class="img-responsive" src="dist/images/logo-2.png" style="max-height: 120px; padding:3px; margin: 10px auto;">
			  	<h2 class="carousel-head" style="text-align: center; color: #fff;">সমৃদ্ধির যাত্রায় আমরা ক’জনা</h2>
			</div>
		</div> -->
	</section>

	<section class="story-bg">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<img class="img-thumbnail" src="dist/images/main.jpg">
				</div>
				<div class="col-md-7">
					<h1>Historical Background</h1>
					<div class="row">
						<div class="col-md-12">							
							<hr class="miss-hr">
						</div>
					</div>
					
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt veritatis doloremque quisquam, itaque veniam nemo quis hic explicabo rerum ipsa, animi magnam autem in fugit, excepturi officia. Quo, perspiciatis aliquid! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas quos ipsam asperiores corporis vero obcaecati alias culpa. Est iure, debitis voluptatum, corporis esse nihil obcaecati, omnis nulla adipisci id expedita!</p>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="top-section-bg">
			<div class="container">
			<h1 style="color:#fff;">Our Enrolment</h1>
			<div class="row">
				<div class="col-md-12">							
					<hr class="miss-hr">
				</div>
			</div>
			</div>
		</div>
		<div class="container class-position">
			<div class="row">
				<div class="col-md-4">
					<a href="#">
						<img class="img-thumbnail" src="{{ asset('dist/images/missio.jpg') }}" alt="">
					</a>
					<h3 class="text-center">Social Mobilization</h3>
				</div>
				<div class="col-md-4">
					<a href="#">
						<img class="img-thumbnail" src="{{ asset('dist/images/missio-3.jpg') }}" alt="">
					</a>
					<h3 class="text-center">Business Activities</h3>
				</div>
				<div class="col-md-4">
					<a href="#">
						<img class="img-thumbnail" src="{{ asset('dist/images/missio-2.jpg') }}" alt="">
					</a>
					<h3 class="text-center">Organizational Events</h3>
				</div>
			</div>
		</div>
	</section>

	{{-- <section>
		<div class="container">
		    <h2 class="service-head" style="font-family: 'Schoolbell', cursive;">OUR BUSINESSES</h2>
		    <hr class="title-hr-2">

		    <div class="row">
		    	<div class="col-md-4">
	    			<img src="{{ asset('dist/images/Car-Motion2.jpg') }}" class="img-responsive">
	    			<a href="{{ url('/services/Transport_fleet_management') }}"><h3 style="text-align: center;  color: #000; font-family: 'Schoolbell', cursive;">Transport fleet management</h3></a>
	    			<p></p>
		    	</div>
		    	<div class="col-md-4">
		    		<img src="{{ asset('dist/images/bazar.jpg') }}" class="img-responsive">
		    		<a href="{{ url('/services/Shondhi_Bazar') }}"><h3 style="text-align: center; color: #000; font-family: 'Schoolbell', cursive;">E-commerce</h3></a>
		    		<p></p>
		    	</div>
		    	<div class="col-md-4">
		    		<img src="{{ asset('dist/images/tomato.jpg') }}" class="img-responsive">
		    		<a href="{{ url('/services/Organic_Food') }}"><h3 style="text-align: center; color: #000;font-family: 'Schoolbell', cursive;">Organic Food</h3></a>
		    		<p></p>
		    	</div>
		    </div>
		</div>
	</section> --}}


{{-- 	<section style="margin-bottom: 40px;">
		<div class="container">
			<h2 class="service-head" style="font-family: 'Schoolbell', cursive;">Wonderful Memories</h2>
			<hr class="title-hr-2">
		    <div class="row">
		        <div class="col-lg-3 col-md-4 col-xs-12 thumb">
		            <a href="{{ asset('dist/images/gallery/gall-2.jpg') }}" class="fancybox" rel="ligthbox">
		                <img  src="{{ asset('dist/images/gallery/gall-2.jpg') }}" class="zoom img-fluid "  alt="Gallery Image">
		            </a>
		        </div>
		        <div class="col-lg-3 col-md-4 col-xs-12 thumb">
		             <a href="{{ asset('dist/images/gallery/gall-1.jpg') }}"  class="fancybox" rel="ligthbox">
		                 <img  src="{{ asset('dist/images/gallery/gall-1.jpg') }}" class="zoom img-fluid"  alt="Gallery Image">
		             </a>
		        </div>
		         
		        <div class="col-lg-3 col-md-4 col-xs-12 thumb">
		             <a href="{{ asset('dist/images/gallery/gall-3.jpg') }}" class="fancybox" rel="ligthbox">
		                 <img  src="{{ asset('dist/images/gallery/gall-3.jpg') }}" class="zoom img-fluid "  alt="Gallery Image">
		             </a>
		        </div>
		         
		        <div class="col-lg-3 col-md-4 col-xs-12 thumb">
		             <a href="{{ asset('dist/images/gallery/gall-4.jpg') }}" class="fancybox" rel="ligthbox">
		                 <img  src="{{ asset('dist/images/gallery/gall-4.jpg') }}" class="zoom img-fluid "  alt="Gallery Image">
		             </a>
		        </div>
		         
		        <div class="col-lg-3 col-md-4 col-xs-12 thumb">
		             <a href="{{ asset('dist/images/gallery/gall-5.jpg') }}" class="fancybox" rel="ligthbox">
		                 <img  src="{{ asset('dist/images/gallery/gall-5.jpg') }}" class="zoom img-fluid "  alt="Gallery Image">
		             </a>
		        </div>
		         
		        <div class="col-lg-3 col-md-4 col-xs-12 thumb">
		             <a href="{{ asset('dist/images/gallery/gall-6.jpg') }}" class="fancybox" rel="ligthbox">
		                 <img  src="{{ asset('dist/images/gallery/gall-6.jpg') }}" class="zoom img-fluid "  alt="Gallery Image">
		             </a>
		        </div>
		         
		        <div class="col-lg-3 col-md-4 col-xs-12 thumb">
		             <a href="{{ asset('dist/images/gallery/gall-7.jpg') }}" class="fancybox" rel="ligthbox">
		                 <img  src="{{ asset('dist/images/gallery/gall-7.jpg') }}" class="zoom img-fluid "  alt="Gallery Image">
		             </a>
		         </div>
		         
		        <div class="col-lg-3 col-md-4 col-xs-12 thumb">
		             <a href="{{ asset('dist/images/gallery/gall-8.jpg') }}" class="fancybox" rel="ligthbox">
		                 <img  src="{{ asset('dist/images/gallery/gall-8.jpg') }}" class="zoom img-fluid "  alt="Gallery Image">
		             </a>
		        </div>
		    </div>
		 </div>
	</section> --}}

@endsection