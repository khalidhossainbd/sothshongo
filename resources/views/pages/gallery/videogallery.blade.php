@extends('layouts.mainlayout')

@section('content')

<section>
	<div class="top-bg-two">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<p class="top-head">Video Gallery</p>
				</div>
			</div>
			
		</div>
	</div>
</section>

<section style="margin: 30px 0;">
	<div class="container">

		<div class="row">
			@if(count($video)>0)

			@foreach($video as $item)
			<div class="col-lg-3 col-md-4 col-xs-6 thumb">
	    	    <a href="#" class="" data-toggle="modal" data-target="#videoModal{{ $item->id }}" data-theVideo="{{ $item->video_url }}">
	    	        <img  src="{{ asset('uploads/videoimg/'.$item->video_img) }}" class="img-thumbnail"  alt="">
	    	    </a>
	    	</div>


			<div class="modal fade" id="videoModal{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
			    <div class="modal-dialog">
			        <div class="modal-content">
			            <div class="modal-body">
			                <button type="button" class="close" style="color: #fff" data-dismiss="modal" aria-hidden="true">&times;</button>
			                <div>
			                    <iframe width="100%" height="450" src=""></iframe>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
			@endforeach
			@else
				<h2 class="message-head text-center">No Video For Gallery</h2>
			@endif
		</div>
		{{-- <h2 class="message-head">Wonderful Memories</h2>
	    <div class="row">
	    	<div class="col-lg-3 col-md-4 col-xs-6 thumb">
	    	    <a href="dist/images/gallery/gall-13.jpg" class="fancybox" rel="ligthbox">
	    	        <img  src="dist/images/gallery/gall-13.jpg" class="zoom img-fluid "  alt="">
	    	    </a>
	    	</div>
	    	<div class="col-lg-3 col-md-4 col-xs-6 thumb">
	    	    <a href="dist/images/gallery/gall-12.jpg" class="fancybox" rel="ligthbox">
	    	        <img  src="dist/images/gallery/gall-12.jpg" class="zoom img-fluid "  alt="">
	    	    </a>
	    	</div>
	    	<div class="col-lg-3 col-md-4 col-xs-6 thumb">
	    	    <a href="dist/images/gallery/gall-11.jpg" class="fancybox" rel="ligthbox">
	    	        <img  src="dist/images/gallery/gall-11.jpg" class="zoom img-fluid "  alt="">
	    	    </a>
	    	</div>
	    	<div class="col-lg-3 col-md-4 col-xs-6 thumb">
	    	    <a href="dist/images/gallery/gall-10.jpg" class="fancybox" rel="ligthbox">
	    	        <img  src="dist/images/gallery/gall-10.jpg" class="zoom img-fluid "  alt="">
	    	    </a>
	    	</div>
	    	<div class="col-lg-3 col-md-4 col-xs-6 thumb">
	    	    <a href="dist/images/gallery/gall-9.jpg" class="fancybox" rel="ligthbox">
	    	        <img  src="dist/images/gallery/gall-9.jpg" class="zoom img-fluid "  alt="">
	    	    </a>
	    	</div>
	        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
	            <a href="dist/images/gallery/gall-2.jpg" class="fancybox" rel="ligthbox">
	                <img  src="dist/images/gallery/gall-2.jpg" class="zoom img-fluid "  alt="">
	            </a>
	        </div>
	        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
	             <a href="dist/images/gallery/gall-1.jpg"  class="fancybox" rel="ligthbox">
	                 <img  src="dist/images/gallery/gall-1.jpg" class="zoom img-fluid"  alt="">
	             </a>
	        </div>
	         
	        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
	             <a href="dist/images/gallery/gall-3.jpg" class="fancybox" rel="ligthbox">
	                 <img  src="dist/images/gallery/gall-3.jpg" class="zoom img-fluid "  alt="">
	             </a>
	        </div>
	         
	        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
	             <a href="dist/images/gallery/gall-4.jpg" class="fancybox" rel="ligthbox">
	                 <img  src="dist/images/gallery/gall-4.jpg" class="zoom img-fluid "  alt="">
	             </a>
	        </div>
	         
	        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
	             <a href="dist/images/gallery/gall-5.jpg" class="fancybox" rel="ligthbox">
	                 <img  src="dist/images/gallery/gall-5.jpg" class="zoom img-fluid "  alt="">
	             </a>
	        </div>
	         
	        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
	             <a href="dist/images/gallery/gall-6.jpg" class="fancybox" rel="ligthbox">
	                 <img  src="dist/images/gallery/gall-6.jpg" class="zoom img-fluid "  alt="">
	             </a>
	        </div>
	         
	        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
	             <a href="dist/images/gallery/gall-7.jpg" class="fancybox" rel="ligthbox">
	                 <img  src="dist/images/gallery/gall-7.jpg" class="zoom img-fluid "  alt="">
	             </a>
	         </div>
	         
	        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
	             <a href="dist/images/gallery/gall-8.jpg" class="fancybox" rel="ligthbox">
	                 <img  src="dist/images/gallery/gall-8.jpg" class="zoom img-fluid "  alt="">
	             </a>
	        </div>
	        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
	             <a href="dist/images/gallery/gall-1.jpg" class="fancybox" rel="ligthbox">
	                 <img  src="dist/images/gallery/gall-1.jpg" class="zoom img-fluid "  alt="">
	             </a>
	        </div>
	        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
	             <a href="dist/images/gallery/gall-5.jpg" class="fancybox" rel="ligthbox">
	                 <img  src="dist/images/gallery/gall-5.jpg" class="zoom img-fluid "  alt="">
	             </a>
	        </div>
	        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
	             <a href="dist/images/gallery/gall-3.jpg" class="fancybox" rel="ligthbox">
	                 <img  src="dist/images/gallery/gall-3.jpg" class="zoom img-fluid "  alt="">
	             </a>
	        </div>
	        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
	             <a href="dist/images/gallery/gall-7.jpg" class="fancybox" rel="ligthbox">
	                 <img  src="dist/images/gallery/gall-7.jpg" class="zoom img-fluid "  alt="">
	             </a>
	        </div>
	    </div> --}}
	 </div>
</section>

@endsection

@section('jscript')

<script type="text/javascript">
	autoPlayYouTubeModal();

	//FUNCTION TO GET AND AUTO PLAY YOUTUBE VIDEO FROM DATATAG
	function autoPlayYouTubeModal() {
	    var trigger = $("body").find('[data-toggle="modal"]');
	    trigger.click(function () {
	        var theModal = $(this).data("target"),
	            videoSRC = $(this).attr("data-theVideo"),
	            videoSRCauto = videoSRC + "?autoplay=1";
	        $(theModal + ' iframe').attr('src', videoSRCauto);
	        $(theModal + ' button.close').click(function () {
	            $(theModal + ' iframe').attr('src', videoSRC);
	        });
	    });
	}
</script>

@endsection