@extends('layouts.mainlayout')

@section('content')

<section>
	<div class="top-bg-two">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<p class="top-head">Memorial Pictures</p>
				</div>
			</div>
			
		</div>
	</div>
</section>

<section style="margin: 30px 0;">
	<div class="container">
	    <div class="row">
	    	@foreach($gallery as $item)
	    	<h2 class="message-head">{{ $item->title }}</h2>
				<hr class="title-hr-2" align="left">
				<div class="row">
					@foreach($item->GalleryImage as $images)
						{{-- {{ $images->title }} --}}
					<div class="col-lg-3 col-md-4 col-xs-6 thumb">
			    	    <a href="{{ asset('uploads/photogallery/'.$images->images) }}" class="fancybox" rel="ligthbox">
			    	        <img  src="{{ asset('uploads/photogallery/'.$images->images) }}" class="zoom img-fluid "  alt="">
			    	    </a>
			    	</div>
			    	@endforeach
				</div>
	    	@endforeach
	    </div>
	 </div>
</section>

@endsection