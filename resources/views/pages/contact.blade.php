@extends('layouts.mainlayout')

@section('content')

	<section>
		<div class="top-bg-one">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<p class="top-head">Contuct US</p>
					</div>
				</div>
				
			</div>
		</div>
	</section>

	<section>
		<div class="container">
			<h2 class="text-center">We are open for your suggestion</h2>
			<div class="row">
				<div class="col-md-6 col-md-offset-4">							
					<hr class="miss-hr">
				</div>
			</div>
			<div class="row" style="margin-bottom: 50px;">
				<div class="col-md-6">
					<form method="post" id="free-call-form">
					<div class="form-group">
					    <label for="name">Name</label>
					    <input type="text" class="form-control" name="name" id="name" placeholder="Name" required="required">
					</div>
					<div class="form-group">
					    <label for="email">Email </label>
					    <input type="email" class="form-control" name="email" id="email" placeholder="Email" required="required">
					</div>
					<div class="form-group">
					    <label for="mobile">Mobile Number</label>
					    <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile Number" required="required">
					</div>
					<div class="form-group">
					    <label for="message">Message</label>
					    <textarea class="form-control" name="message" rows="5" required="required"></textarea>
					</div>
					<button type="submit" class="btn btn-primary btn-block" id="btnContactUs">Submit</button>
					</form>
				</div>

				<div class="col-md-6" style="padding: 30px 60px;">
					<p class="add-head">Office Address: </p>
					<p class="add-text">
						House# 241, Block# B,<br>
						Ground Floor,<br>
						Eastern Housing,<br>
						Pallabi (2nd Phase),<br>
						Dhaka-1212<br>
					</p>
				</div>
			</div>
		</div>
	</section>

@endsection