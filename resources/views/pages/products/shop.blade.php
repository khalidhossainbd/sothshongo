@extends('layouts.mainlayout')

@section('content')

<section>
	<div class="top-bg-two">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<p class="top-head">Our Shop</p>
				</div>
			</div>
			
		</div>
	</div>
</section>

<section style="margin: 30px 0;">

	<div class="container">
		<h2>Our Products List</h2>
		<div class="row">
			<div class="col-md-12">							
				<hr class="miss-hr">
			</div>
		</div>
		
	    <div class="row">
	    	<div class="col-xs-12 col-sm-6 col-md-3">
	    		<img class="img-thumbnail" src="{{ asset('/images/fish.jpg') }}" alt="">
	    		<h4>Koi(biler)</h4>
	    		<p>1 KG 220.00 (BDT)</p>
	    	</div>
	    	<div class="col-xs-12 col-sm-6 col-md-3">
	    		<img class="img-thumbnail" src="{{ asset('/images/egg.jpg') }}" alt="">
	    		<h4>Chicken Eggs Loose</h4>
	    		<p>12pcs 120.00 (BDT)</p>
	    	</div>
	    	<div class="col-xs-12 col-sm-6 col-md-3">
	    		<img class="img-thumbnail" src="{{ asset('/images/chill.jpg') }}" alt="">
	    		<h4>Chilli Powder</h4>
	    		<p>250gm 50.00 (BDT)</p>
	    	</div>
	    	<div class="col-xs-12 col-sm-6 col-md-3">
	    		<img class="img-thumbnail" src="{{ asset('/images/oil.jpeg') }}" alt="">
	    		<h4>Mustard Oil (Ghani)</h4>
	    		<p>1 Litter 220.00 (BDT)</p>
	    	</div>
	    </div>
	 </div>
</section>

@endsection