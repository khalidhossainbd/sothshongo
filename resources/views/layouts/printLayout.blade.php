<!DOCTYPE html>
<html>
<head>
	<title>Transaction Print</title>
	<script src="{{ asset('assets/admin/theme/bower_components/jquery/dist/jquery.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}">
  	<script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

	<script type="text/javascript">
  	var siteURL="{{ url('') }}"
	</script>

	<style type="text/css">
		
		.table tr{
			width: 100%;
		}
		.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
			padding: 3px !important;
			border-top: none !important;
			vertical-align: middle !important;
		}

	</style>
</head>
<body>
	<div class="container" style="">
		<table class="table">
			<tr>
				<td width="24%">
					<img style="height: 100px;" src="{{ asset('dist/images/logo-3.png') }}">
				</td>
				<td width="45%" style="float: right;">
					<table>
						<tr>
							<td colspan="3" style="font-size: 20px;"><b>Sothshongo Shomobai</b></td>
						</tr>
						<tr>
							<td style="font-size: 15px"></td>
							
							<td style="font-size: 13px;">
								House# 241, Block#B, Eastern Housing<br/>
								Pallabi, Mirpur, Dhaka-1206

							</td>
						</tr>
						<tr>
							<td style="font-size: 15px"></td>
							<td style="font-size: 13px;">
								 +88 01720279279
							</td>
					</table>
				</td>
				<td>
					
				</td>
			</tr>
		</table>
		<!-- <center><h3><b>@yield('head')</b></h3></center> -->
		@yield('content')

		<table class="table" style="border:none">
			<tr>
			    <td>
			        <span class="pull-left" style="margin-top: 30px;border-top: 1px solid gray;">Accountants</span>
			    </td>
			    <td>
			        <span class="pull-right" style="margin-top: 30px; border-top: 1px solid gray;">Authorized Signature</span>
			    </td>
				<td><span class="pull-right" style="margin-top: 30px;border-top: 1px solid gray;">Prepared By <br> {{ Auth::user()->name  }}</span></td>
			</tr>
		</table>
	</div>

	<script type="text/javascript">
		$(window).on("load", function() {
        	window.print();
		  	setTimeout(window.close,1)
		});
	</script>
	
</body>
</html>