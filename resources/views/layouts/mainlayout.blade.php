<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>
		@if(View::hasSection('title'))
	       SothShongo | @yield('title')
	    @else
	        SothShongo Co-operative Organization
	    @endif
	</title>
	<meta name="description" content="SothShongo Co-operative Organization | ‘SothShongo’, a Bangla word, means' Joining’">
	<meta name="keywords" content="SothShongo ,SothShongo Co-operative, ">
	<meta name="author" content="Md Khalid Hossain">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" href="{{ asset('dist/images/logo-3.png') }}" type="image/gif" sizes="16x16">

	<link rel="stylesheet" type="text/css" href="{{ asset('dist/lib/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('dist/lib/font-awesome/css/font-awesome.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('dist/lib/animate-css/animate.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/bangla.css') }}">

	<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/custom.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/style.css') }}">

	
</head>
<body>

	@include('partials.mainmenu')


	@yield('content')


	@include('partials.mainfooter')



	<script type="text/javascript" src="{{ asset('dist/lib/jquery/jquery.js') }}"></script>
	<script type="text/javascript" src="{{ asset('dist/lib/bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/snap.svg/0.3.0/snap.svg-min.js"></script>

	 <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
	<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>

	<script type="text/javascript">
		$(function(){
		 $(".dropdown").hover(            
	         function() {
	             $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
	             $(this).toggleClass('open');
	             $('b', this).toggleClass("caret caret-up");                
	         },
	         function() {
	             $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
	             $(this).toggleClass('open');
	             $('b', this).toggleClass("caret caret-up");                
	         });
		 });
	</script>

	<script type="text/javascript">
		$(document).ready(function() {
		           $(".menu-icon").on("click", function() {
		                 $("nav ul").toggleClass("showing");
		           });
		     });

		     // Scrolling Effect

		     $(window).on("scroll", function() {
		           if($(window).scrollTop()) {
		                 $('nav').addClass('black');
		           }

		           else {
		                 $('nav').removeClass('black');
		           }
		     });
	</script>

	<script type="text/javascript">
		$('#myCarousel').carousel();
		var winWidth = $(window).innerWidth();
		$(window).resize(function () {

		    if ($(window).innerWidth() < winWidth) {
		        $('.carousel-inner>.item>img').css({
		            'min-width': winWidth, 'width': winWidth
		        });
		    }
		    else {
		        winWidth = $(window).innerWidth();
		        $('.carousel-inner>.item>img').css({
		            'min-width': '', 'width': ''
		        });
		    }
		});
	</script>

	<script type="text/javascript">
		$(document).ready(function(){
		  $(".fancybox").fancybox({
		        openEffect: "none",
		        closeEffect: "none"
		    });
		    
		    $(".zoom").hover(function(){
				
				$(this).addClass('transition');
			}, function(){
		        
				$(this).removeClass('transition');
			});
		});
	</script>

	@yield('jscript')
	
</body>
</html>