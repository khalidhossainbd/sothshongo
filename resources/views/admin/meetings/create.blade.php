@extends('layouts.dashlayout')

@section('content')
  
   <div class="container-fluid">
       <h3 class="page-title">Add New Meeting Minutes</h3>
       <div class="row">
           <div class="col-md-12">
               <div class="panel">
                   <div class="panel-heading">
                       @if ($errors->any())
                           <ul class="alert alert-danger">
                               @foreach ($errors->all() as $error)
                                   <li>{{ $error }}</li>
                               @endforeach
                           </ul>
                       @endif
                   </div>
                   <div class="panel-body">
                       <form method="POST" action="{{ url('/kadmin/meeting-minutes') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                           {{ csrf_field() }}
                       <div class="row">
                           <div class="col-md-6">
                               <div class="form-group">
                                   <label for="title">Title</label>
                                   <input type="text" class="form-control" id="title" name="title" placeholder="Title" required="required">
                               </div>
                               <div class="form-group">
                                   <label for="author">Publish By</label>
                                   <input type="text" class="form-control" id="author" name="author" placeholder="Author" required="required">
                               </div>
                               
                               <div class="form-group">
                                   <label for="publishDate">Publish Date</label>
                                   <input type="date" class="form-control" id="publishDate" name="publishDate" placeholder="Publish Date">
                               </div>
                               <div class="form-group">
                                   <label for="name">Status</label>
                                   <select class="form-control" name="status" required="required">
                                       <option value="">Select an Option</option>
                                       <option value="1">Active</option>
                                       <option value="0">Inactive</option>
                                   </select>
                               </div>
                               <div class="form-group">
                                   <label for="name">Content</label>
                                   <textarea class="form-control" rows="5" name="content"></textarea>
                               </div>
                               <div class="form-group">
                                   <button type="submit" class="btn btn-success">Save</button>
                                   <button type="reset" class="btn btn-info">Reset</button>
                               </div>
                               
                           </div>
                           <div class="col-md-6">
                           	
                           </div>
                           
                       </div>
                       </form>
                   </div>
               </div>
           </div>
       </div>
   </div>
    
@endsection

@section('java_script')

<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  <script>tinymce.init({selector:'textarea'});</script>

@endsection
