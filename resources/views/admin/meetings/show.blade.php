@extends('layouts.dashlayout')

@section('content')
  
    
<div class="container-fluid">
    <h3 class="page-title">Meeting Minutes Details</h3>
    <a class="btn btn-primary" href="{{ url('/kadmin/meeting-minutes') }}">Back</a> 
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    @if(Session::has('flash_message'))
					    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
					@endif
                </div>
                <div class="panel-body">
                    <div class="row">
                    	<div class="col-md-8">
                    	<table class="table table-bordered table-hover">
                    		<tr>
                    			<th>Title</th>
                    			<td>{{ $data->title }}</td>
                    		</tr>
                    		<tr>
                    			<th>Order By</th>
                    			<td>{{ $data->author }}</td>
                    		</tr>
                    		<tr>
                    			<th>Publish Date</th>
                    			<td>{{ $data->publishDate }}</td>
                    		</tr>
                    		<tr>
                    			<th>Status</th>
                    			<td>
                    				@php 
                    				    if($data->status >0){
                    				        echo "<p style='color:green'>Active</p>";
                    				    }else{
                    				        echo "<p style='color:red'>Inactive</p>";
                    				    }
                    				@endphp
                    			</td>
                    		</tr>
                    		<tr>
                    			<th>Content</th>
                    			<td>{!! $data->content !!}</td>
                    		</tr>
                    	</table>
                    	</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
