@extends('layouts.dashlayout')

@section('content')
  
    
<div class="container-fluid">
    <h3 class="page-title">Meeting Minutes List</h3>
    <a class="btn btn-primary" href="{{ url('/kadmin/meeting-minutes/create') }}">Add New</a> 
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    @if(Session::has('flash_message'))
					    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
					@endif
                </div>
                <div class="panel-body">
                    <div class="row">
                    	<table class="table table-bordered table-hover">
                    		<thead>
	                    		<tr>
	                    			<th>Sl No</th>
	                    			<th>Title</th>
	                    			<th>Order By</th>
	                    			<th>Publish Date</th>
	                    			<th>Status</th>
	                    			<th>Action</th>
	                    		</tr>
                    		</thead>
                    		<tbody>
                    			@if(count($data)>0)
	                    			@foreach($data as $item)
	                    				<tr>
	                    					<td>{{ $loop->iteration }}</td>
	                    					<td>{{ $item->title }}</td>
	                    					<td>{{ $item->author }}</td>
	                    					<td>{{ $item->publishDate }}</td>
	                    					
	                    					<td>
												@php 
												    if($item->status >0){
												        echo "<p style='color:green'>Active</p>";
												    }else{
												        echo "<p style='color:red'>Inactive</p>";
												    }
												@endphp
	                    					</td>
	                    					<td>
	                    						<a href="{{ url('/kadmin/meeting-minutes/' . $item->id) }}" title="View Poem"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
	                    						<a href="{{ url('/kadmin/meeting-minutes/' . $item->id . '/edit') }}" title="Edit Poem"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
	                    					</td>
	                    				</tr>
	                    			@endforeach
                    			@else
                    			<p>No data found</p>
                    			@endif
                    		</tbody>

                    	</table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
