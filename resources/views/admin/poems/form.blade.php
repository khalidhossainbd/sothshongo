<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="col-md-4 control-label">{{ 'Title' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="title" type="text" id="title" value="{{ $poem->title or ''}}" >
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('author') ? 'has-error' : ''}}">
    <label for="author" class="col-md-4 control-label">{{ 'Author' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="author" type="text" id="author" value="{{ $poem->author or ''}}" >
        {!! $errors->first('author', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('publishDate') ? 'has-error' : ''}}">
    <label for="publishDate" class="col-md-4 control-label">{{ 'Publishdate' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="publishDate" type="date" id="publishDate" value="{{ $poem->publishDate or ''}}" >
        {!! $errors->first('publishDate', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('poemImg') ? 'has-error' : ''}}">
    <label for="poemImg" class="col-md-4 control-label">{{ 'Poemimg' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="poemImg" type="file" id="poemImg" value="{{ $poem->poemImg or ''}}" >
        {!! $errors->first('poemImg', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('contentBangla') ? 'has-error' : ''}}">
    <label for="contentBangla" class="col-md-4 control-label">{{ 'Contentbangla' }}</label>
    <div class="col-md-6">
        <textarea class="form-control" rows="5" name="contentBangla" type="textarea" id="contentBangla" >{{ $poem->contentBangla or ''}}</textarea>
        {!! $errors->first('contentBangla', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('contentEnglist') ? 'has-error' : ''}}">
    <label for="contentEnglist" class="col-md-4 control-label">{{ 'Contentenglist' }}</label>
    <div class="col-md-6">
        <textarea class="form-control" rows="5" name="contentEnglist" type="textarea" id="contentEnglist" >{{ $poem->contentEnglist or ''}}</textarea>
        {!! $errors->first('contentEnglist', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>
