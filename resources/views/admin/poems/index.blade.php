@extends('layouts.dashlayout')

@section('content')
  
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <h2 class="card-header">Memorable Poems</h2>
                <hr>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="{{ url('/kadmin/poems/create') }}" class="btn btn-success btn-sm" title="Add New Poem">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add New
                            </a>
                        </div>
                        <div class="col-md-3 col-md-offset-2">
                            <form method="GET" action="{{ url('/kadmin/poems') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                              <div class="form-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                              </div>
                              
                              <button type="submit" class="btn btn-success" style="margin-top: 5px;"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                    <br><br>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sl No</th>
                                    <th>Title</th>
                                    <th>Author</th>
                                    <th>Publish Date</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($poems as $item)
                                <tr>
                                    <td>{{ $loop->iteration or $item->id }}</td>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->author }}</td>
                                    <td>{{ $item->publishDate }}</td>
                                    <td>
                                        <a href="{{ url('/kadmin/poems/' . $item->id) }}" title="View Poem"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                        <a href="{{ url('/kadmin/poems/' . $item->id . '/edit') }}" title="Edit Poem"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                        <form method="POST" action="{{ url('/kadmin/poems' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Poem" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $poems->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    
@endsection
