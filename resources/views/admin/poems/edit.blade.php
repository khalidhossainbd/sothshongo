@extends('layouts.dashlayout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <h2 class="card-header">Edit Poem</h2>
                <hr>
                <div class="card-body">
                    <a href="{{ url('/kadmin/poems') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <br />
                    <br />

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                        {{-- @include ('admin.poems.form', ['submitButtonText' => 'Update']) --}}

                <div class="row">
                    <div class="col-md-6">
                        <form method="POST" action="{{ url('/kadmin/poems/' . $poem->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}

                        {{-- @include ('admin.poems.form') --}}
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                            <label for="Title">Poem Title</label>
                            <input type="text" class="form-control" id="title" value="{{ $poem->title }}" name="title">
                            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {{ $errors->has('author') ? 'has-error' : ''}}">
                            <label for="author">Author Name</label>
                            <input type="text" class="form-control" id="author" value="{{ $poem->author }}" name="author">
                            {!! $errors->first('author', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {{ $errors->has('publishDate') ? 'has-error' : ''}}">
                            <label for="publishDate">Publish Date</label>
                            <input class="form-control" name="publishDate" type="date" id="publishDate" value="{{ $poem->publishDate }}" >
                            {!! $errors->first('publishDate', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group {{ $errors->has('contentBangla') ? 'has-error' : ''}}">
                            <label for="contentBangla">Poem Bangla Text</label>
                            <textarea name="contentBangla" id="contentBangla" type="textarea" class="form-control" rows="8">
                                {!! $poem->contentBangla !!}
                            </textarea>
                            {!! $errors->first('contentBangla', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('contentEnglist') ? 'has-error' : ''}}">
                            <label for="contentEnglist">Poem English Text</label>
                            <textarea name="contentEnglist" id="contentEnglist" type="textarea" class="form-control" rows="8">
                                {!! $poem->contentEnglist !!}
                            </textarea>
                            {!! $errors->first('contentEnglist', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {{ $errors->has('poemImg') ? 'has-error' : ''}}" >
                            <label for="poemImg">Upload Poem Related Image</label>
                            <input type="file" id="poemImg" name="poemImg">
                            <p class="help-block">Image size 400px X 400px</p>
                            {!! $errors->first('poemImg', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <button type="reset" class="btn btn-info">Reset</button>
                        </div>
                    </div>
                    </form>
                </div>


                </div>
            </div>
        </div>
    </div>
@endsection
