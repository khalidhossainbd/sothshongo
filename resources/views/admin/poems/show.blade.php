@extends('layouts.dashlayout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">

                    <a href="{{ url('/kadmin/poems') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <a href="{{ url('/kadmin/poems/' . $poem->id . '/edit') }}" title="Edit Poem"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                    <form method="POST" action="{{ url('kadmin/poems' . '/' . $poem->id) }}" accept-charset="UTF-8" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger btn-sm" title="Delete Poem" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                    </form>
                    <br/>
                    <br/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        
                                        <tr><th>Poem Title </th>
                                            <td> {{ $poem->title }} </td></tr>
                                        <tr><th> Author Name </th>
                                            <td> {{ $poem->author }} </td></tr>
                                        <tr><th> Publish Date </th>
                                            <td> {{ $poem->publishDate }} </td></tr>
                                        <tr><th> Poem Bangla </th>
                                            <td> {!! $poem->contentBangla !!} </td></tr>
                                        <tr><th> Poem English </th>
                                            <td> {!! $poem->contentEnglist !!} </td></tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-4 col-md-offset-1">
                            <tr><th>Poem Image </th>
                                <td>@php
                                    if(!($poem->poemImg))
                                        { 
                                    @endphp
                                        <br>No Image
                                    @php
                                    }else{
                                    @endphp
                                        <img class="img-responsive" src="{{ asset('uploads/containImg/'.$poem->poemImg) }} " alt="img" style="max-height: 200px;"> </td>
                                    @php
                                    }
                                    @endphp

                                    </tr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
