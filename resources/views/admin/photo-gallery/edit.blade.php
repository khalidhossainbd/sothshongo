@extends('layouts.dashlayout')

@section('content')


    <div class="container-fluid">
        <!-- OVERVIEW -->
        <div class="panel panel-headline">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                        <h3 class="panel-title">Edit Photo gallery</h3>
                        <a class="btn btn-warning btn-sm" href="{{ url('/kadmin/photo-gallery') }}" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back</a>
                        </div>
                    </div>
                    <div class="col-md-6"> 
                        @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
                </div>
            </div>
            
            <div class="panel-body">
                <div class="row">
                    <form method="POST" action="{{ url('/kadmin/photo-gallery/' . $gallery->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title">Gallery Title</label>
                            <input type="text" class="form-control" id="title" value="{{ $gallery->title }}" name="title" required="required">
                        </div>
                        <div class="form-group">
                            <label for="address">Date</label>
                            <input type="date" class="form-control" id="title" value="{{ $gallery->date }}" name="date" required="required">
                        </div>
                        <div class="form-group">
                            <label for="address">Serial Date</label>
                            <input type="date" class="form-control" id="title" value="{{ $gallery->sl_date }}" name="sl_date" required="required">
                        </div>
                        <div class="form-group">
                            <label for="content">Content</label>
                            <textarea class="form-control" rows="5" name="content">{{ $gallery->content }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Status</label>
                            <select class="form-control" id="status" name="status" required="required">
                                <option>Select Option</option>
                                @php
                                if($gallery->status == 1){
                                @endphp
                                    <option selected="selected" value="1">Publish</option>
                                    <option value="0">Unpublish</option>
                                @php
                                }else{
                                @endphp
                                    <option value="1">Publish</option>
                                    <option selected="selected" value="0">Unpublish</option>
                                @php
                                }
                                @endphp
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Update">
                        </div>
                        
                    </div>
                   {{--  <div class="col-md-6"></div> --}}
                    </form>
                </div>
            </div>
        </div>
        <!-- END OVERVIEW -->
    </div>
    
    
@endsection
