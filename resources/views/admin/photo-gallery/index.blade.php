@extends('layouts.dashlayout')

@section('content')


    <div class="container-fluid">
        <!-- OVERVIEW -->
        <div class="panel panel-headline">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="panel-title">Photo Gallery List</h3>
                        <a href="{{ url('/kadmin/photo-gallery/create') }}" class="btn btn-success btn-sm" title="Add New Branch">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </div>
                    <div class="col-md-6">

                        <a href="{{ url('/kadmin/gallery-images') }}" class="btn btn-primary btn-sm" title="Add New Branch">
                            <i class="fa fa-list" aria-hidden="true"></i> Image List
                        </a>
                        <a href="{{ url('/kadmin/gallery-images/create') }}" class="btn btn-info btn-sm" title="Add New Branch">
                            <i class="fa fa-plus-square-o" aria-hidden="true"></i> Add New Image
                        </a>
                        {{-- <form method="GET" action="{{ url('/kadmin/branches') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-btn"><button class="btn btn-success btn-sm" type="button">Search</button></span>
                            </div>
                        </form> --}}
                    </div>
                </div>
                
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Sl No.</th>
                                                <th>Gallery Title</th>
                                                <th>Event Date</th>
                                                <th>Description</th>
                                                <th>Serial Date</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($myphoto as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->title }}</td>
                                                <td>{{ $item->date }}</td>
                                                <td>{{ $item->content }}</td>
                                                <td>{{ $item->sl_date }}</td>
                                                <td>
                                                    @php 
                                                        if($item->status >0){
                                                            echo "Publish";
                                                        }else{
                                                            echo"Unpublish";
                                                        }
                                                    @endphp
                                                </td>
                                                <td>
                                                    <a href="{{ url('/kadmin/photo-gallery/' . $item->id) }}" title="View Images"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View Images</button></a>
                                                    <a href="{{ url('/kadmin/photo-gallery/' . $item->id . '/edit') }}" title="Edit Branch"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                                    {{-- <form method="POST" action="{{ url('/kadmin/branches' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-danger btn-sm" title="Delete Branch" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                                    </form> --}}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div class="pagination-wrapper"> {!! $myphoto->appends(['search' => Request::get('search')])->render() !!} </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END OVERVIEW -->
    </div>
        

@endsection
