
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <div>
        <h1 style="text-align: center">Shondhi Somobai</h1>
        
        <p style="text-align: center">Road# 28, Block# K,House# 6(Level #3),
            Beside Banani Puja Math,Dhaka-1206<br>
            Email: info@shondhisomobai.com</p>
    </div>
    <hr>
    <div width="60%">
        <table class="table">
            <tbody>
                <tr><th>Name </th>
                    <td> {{ $applicant->name }} </td></tr>
                <tr><th>Father Name </th>
                    <td> {{ $applicant->f_name }} </td></tr>
                <tr><th>Mother Name </th>
                    <td> {{ $applicant->m_name }} </td></tr>
                <tr><th>Date of Birth </th>
                    <td> {{ $applicant->date }} </td></tr>
                <tr><th> Email </th>
                    <td> {{ $applicant->email }} </td></tr>
                <tr><th> Mobile Number </th>
                    <td> {{ $applicant->mobile }} </td></tr>
                <tr><th> Occupation </th>
                    <td> {{ $applicant->occupation }} </td></tr>
                <tr><th> National ID </th>
                    <td> {{ $applicant->nid }} </td></tr>
                <tr><th> Address </th>
                    <td> {!! $applicant->address !!} </td></tr>
                <tr><th> Nominee Name </th>
                    <td> {!! $applicant->n_name !!} </td></tr>
                <tr><th> Relation With Nominee </th>
                    <td> {!! $applicant->n_relation !!} </td></tr>
                <tr><th> Nationality </th>
                    <td> {!! $applicant->nationality !!} </td></tr>
                <tr><th> Status </th>
                    <td>
                        @php 
                            if($applicant->status==0){
                                echo "<p class='text-success'>New Application</p>";
                            }else{
                                echo "<p class='text-success'>Waiting</p>";
                            }
                        @endphp
                    </td>
                </tr>
                <tr>
                    <th>Applicant Date</th>
                    <td>{{ $applicant->created_at->format('jS F Y') }}</td>
                </tr>
            </tbody>
        </table>
    </div>
    {{-- <div width="40%">
        <img  src="{{ asset('uploads/members/new/'.$applicant->myimage) }}" alt="img" style="max-height: 200px;"> 
    </div> --}}
    
</body>
</html>
    




=