@extends('layouts.dashlayout')

@section('content')


<div class="container-fluid">
    <h3 class="page-title">Applicant Info Update</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <a class="btn btn-warning btn-sm" href="{{ url('/kadmin/applications') }}" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                        </div>
                        <div class="col-md-6">
                            @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>

                    
                </div>
                <div class="panel-body">
                    
                    <form method="POST" action="{{ url('/kadmin/applications/' . $applicant->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6 col-md-offset-2">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ $applicant->name }}" disabled="disabled">
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" value="{{ $applicant->email }}" disabled="disabled">
                            </div>
                            <div class="form-group">
                                <label for="mobile">Mobile Number</label>
                                <input type="text" class="form-control" id="mobile" name="mobile" value="{{ $applicant->mobile }}" disabled="disabled">
                            </div>
                            <div class="form-group">
                                <label for="name">Applicant Status</label>
                                <select class="form-control" name="status">
                                    <option value="">Select an Option</option>
                                    @if($applicant->status == 0)
                                    <option selected="selected" value="0">New Applicant</option>
                                    <option value="1">Waiting</option>
                                    <option value="2">Approved</option>
                                    <option value="3">Reject</option>
                                    @elseif($applicant->status == 1)
                                    <option value="0">New Applicant</option>
                                    <option selected="selected" value="1">Waiting</option>
                                    <option  value="2">Approved</option>
                                    <option value="3">Reject</option>
                                    @elseif($applicant->status == 2)
                                    <option value="0">New Applicant</option>
                                    <option value="1">Waiting</option>
                                    <option selected="selected" value="2">Approved</option>
                                    <option value="3">Reject</option>
                                    @elseif($applicant->status == 3)
                                    <option value="0">New Applicant</option>
                                    <option value="1">Waiting</option>
                                    <option value="2">Approved</option>
                                    <option selected="selected" value="3">Reject</option>
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">Short Note</label>
                                <textarea class="form-control" rows="3" name="content">{{ $applicant->note }}</textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Update</button>
                                
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
