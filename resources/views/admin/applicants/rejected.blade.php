@extends('layouts.dashlayout')

@section('content')


    <div class="container-fluid">
        <!-- OVERVIEW -->
        <div class="panel panel-headline">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="panel-title">Rejected Applicants List</h3>
                        {{-- <a href="{{ url('/kadmin/photo-gallery/create') }}" class="btn btn-success btn-sm" title="Add New Branch">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a> --}}
                    </div>
                    <div class="col-md-6">

                        
                    </div>
                </div>
                
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Sl No.</th>
                                                <th>Name</th>
                                                <th>Mobile</th>
                                                <th>Email</th>
                                                <th>Occupation</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($applicants as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->mobile }}</td>
                                                <td>{{ $item->email }}</td>
                                                <td>{{ $item->occupation }}</td>
                                                <td>
                                                    @php 
                                                        if($item->status==0){
                                                            echo "<p class='text-danger'>New Application</p>";
                                                        }else{
                                                            echo "<p class='text-success'>Waiting</p>";
                                                        }
                                                    @endphp
                                                </td>
                                                <td>
                                                    <a href="{{ url('/kadmin/applications/' . $item->id) }}" title="View Images"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View Details</button></a>
                                                    <a href="{{ url('/kadmin/applications/' . $item->id . '/edit') }}" title="Edit Branch"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Change Status</button></a>

                                                    {{-- <form method="POST" action="{{ url('/kadmin/branches' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-danger btn-sm" title="Delete Branch" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                                    </form> --}}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{-- <div class="pagination-wrapper"> {!! $myphoto->appends(['search' => Request::get('search')])->render() !!} </div> --}}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END OVERVIEW -->
    </div>
        

@endsection
