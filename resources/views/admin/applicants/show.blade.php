@extends('layouts.dashlayout')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel">
        	<div class="panel-header">
        		<h3 class="page-title">Applicant Info Details</h3>
        	</div>
            <div class="panel-body">

                <a href="{{ url('/kadmin/applications') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <a href="{{ url('/kadmin/applications/pdfdownload/'.$applicant->id ) }}" title="Edit Poem"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> PDF Download</button></a>

                {{-- <form method="POST" action="{{ url('kadmin/members' . '/' . $member->id) }}" accept-charset="UTF-8" style="display:inline">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Poem" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                </form> --}}
                <br/>
                <br/>
                <div class="row">
                    <div class="col-md-6">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    
                                    <tr><th>Name </th>
                                        <td> {{ $applicant->name }} </td></tr>
                                    <tr><th>Father Name </th>
                                        <td> {{ $applicant->f_name }} </td></tr>
                                    <tr><th>Mother Name </th>
                                        <td> {{ $applicant->m_name }} </td></tr>
                                    <tr><th>Date of Birth </th>
                                        <td> {{ $applicant->date }} </td></tr>
                                    <tr><th> Email </th>
                                        <td> {{ $applicant->email }} </td></tr>
                                    <tr><th> Mobile Number </th>
                                        <td> {{ $applicant->mobile }} </td></tr>
                                    <tr><th> Occupation </th>
                                        <td> {{ $applicant->occupation }} </td></tr>
                                    <tr><th> National ID </th>
                                        <td> {{ $applicant->nid }} </td></tr>
                                    <tr><th> Address </th>
                                        <td> {!! $applicant->address !!} </td></tr>
                                    <tr><th> Nominee Name </th>
                                        <td> {!! $applicant->n_name !!} </td></tr>
                                    <tr><th> Relation With Nominee </th>
                                        <td> {!! $applicant->n_relation !!} </td></tr>
                                    <tr><th> Nationality </th>
                                        <td> {!! $applicant->nationality !!} </td></tr>
                                    <tr><th> Status </th>
                                        <td>
                                            @php 
                                                if($applicant->status==0){
                                                    echo "<p class='text-success'>New Application</p>";
                                                }else{
                                                    echo "<p class='text-success'>Waiting</p>";
                                                }
                                            @endphp
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Applicant Date</th>
                                        <td>{{ $applicant->created_at->format('jS F Y') }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <tr><th>Member Image </th>
                            <td>
                            	<img class="img-responsive" src="{{ asset('uploads/members/new/'.$applicant->myimage) }}" alt="img" style="max-height: 200px;"> 
                            </td>
                        </tr>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection