@extends('layouts.dashlayout')

@section('content')


    <div class="container-fluid">
        <!-- OVERVIEW -->
        <div class="panel panel-headline">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="panel-title">Add New Images for Gallery</h3>
                        <a class="btn btn-warning btn-sm" href="{{ url('/kadmin/gallery-images') }}" title="Back"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back</a>
                    </div>
                    <div class="col-md-6"> 
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
            
            <div class="panel-body">
                <div class="row">
                    <form method="POST" action="{{ url('/kadmin/gallery-images') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title">Image Title</label>
                            <input type="text" class="form-control" id="title" placeholder="Image Title" name="title" required="required">
                        </div>
                        
                        <div class="form-group">
                            <label for="branch_id">Gallery Name</label>
                            <select class="form-control" id="" name="photogallery_id" required="required">
                                <option>Select Option</option>
                                @foreach($photogallery as $item)
                                <option value="{{ $item->id }}">{{ $item->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="files">File input</label>
                            <input type="file" id="files" name="images[]" multiple="multiple">
                            <p class="help-block">Image size must be 940 x 629 PX</p>
                        </div>
                        
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Save">
                        </div>
                    </div>
                    <div class="col-md-6" style="padding: 0 50px;">
                        <div class="form-group">
                            <label>Selected Image</label>
                            <img style="max-height: 300px;" class="img-responsive" id="image" />
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection
