@extends('layouts.dashlayout')

@section('content')
  
    
<div class="container-fluid">
    <h3 class="page-title">Subscription List</h3>
    <a class="btn btn-primary" href="{{ route('payment.create') }}">Add New</a> 
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    @if(Session::has('flash_message'))
					    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
					@endif
                </div>
                <div class="panel-body">
                    <div class="row">
                    	<table class="table table-bordered table-hover">
                    		<thead>
	                    		<tr>
	                    			<th>Sl No</th>
	                    			<th>Title</th>
	                    			<th>Title</th>
	                    			<th>Amount (BDT)</th>
	                    			<th>Date</th>
	                    			
	                    			<th>Action</th>
	                    		</tr>
                    		</thead>
                    		<tbody>
                    			 @if(count($data)>0)
	                    			@foreach($data as $item)
	                    				<tr>
	                    					<td>{{ $loop->iteration }}</td>
	                    					<td>{{ $item->member->name }}</td>
	                    					<td>{{ $item->paymentType->title }}</td>
	                    					<td>{{ $item->amount }}</td>
	                    					<td>{{ $item->date }}</td>
	                    					<td>
	                    						<a href="{{ url('/kadmin/payment/' . $item->id . '/edit') }}" title="Edit Poem"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
	                    						<a href="{{ url('/kadmin/payment/print/' . $item->id) }}" title="Edit Poem"><button class="btn btn-danger btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Print</button></a>
                                                
	                    					</td>
	                    				</tr>
	                    			@endforeach
                    			@else
                    			<p>No data found</p>
                    			@endif 
                    		</tbody>

                    	</table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


