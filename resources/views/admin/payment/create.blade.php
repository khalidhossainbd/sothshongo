@extends('layouts.dashlayout')

@section('content')
  
   <div class="container-fluid">
       <h3 class="page-title">Resive new Payment</h3>
       <div class="row">
           <div class="col-md-12">
               <div class="panel">
                   <div class="panel-heading">
                       @if ($errors->any())
                           <ul class="alert alert-danger">
                               @foreach ($errors->all() as $error)
                                   <li>{{ $error }}</li>
                               @endforeach
                           </ul>
                       @endif
                   </div>
                   <div class="panel-body">
                       <form method="POST" action="{{ route('payment.store') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                           {{ csrf_field() }}
                       <div class="row">
                           <div class="col-md-6">
                              <input type="text" name="user_id" style="display: none;" value="{{ Auth::user()->id }}">
                              <div class="form-group">
                                   <label for="name">Payment Type</label>
                                   <select class="form-control" name="payment_type_id" required="required">
                                       <option value="">Select an Option</option>
                                       @foreach($type as $types)
                                       <option value="{{ $types->id }}">{{ $types->title }}</option>
                                       @endforeach
                                   </select>
                               </div>
                               <div class="form-group">
                                   <label for="name">Payment For</label>
                                   <select class="form-control" name="member_id" required="required">
                                       <option value="">Select an Option</option>
                                       @foreach($members as $member)
                                       <option value="{{ $member->id }}">{{ $member->name }}</option>
                                       @endforeach
                                   </select>
                               </div>
                               <div class="form-group">
                                   <label for="amount">Amount</label>
                                   <input type="text" class="form-control" id="amount" name="amount" placeholder="Amount" required="required">
                               </div>
                               
                               <div class="form-group">
                                   <label for="date">Date</label>
                                   <input type="date" class="form-control" id="date" name="date" placeholder="Date">
                               </div>
                               
                               <div class="form-group">
                                   <label for="name">Details</label>
                                   <textarea class="form-control" rows="5" name="content"></textarea>
                               </div>
                               <div class="form-group">
                                   <button type="submit" class="btn btn-success">Save</button>
                                   <button type="reset" class="btn btn-info">Reset</button>
                               </div>
                               
                           </div>
                           <div class="col-md-6">
                           	
                           </div>
                           
                       </div>
                       </form>
                   </div>
               </div>
           </div>
       </div>
   </div>
    
@endsection
