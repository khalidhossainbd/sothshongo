@extends('layouts.printLayout')

@section('content')
  
    
<div class="container-fluid">
    <h3 class="page-title text-center">Invoice</h3>
    <br>
    <p style="float: right;">Date: {{ date("Y/m/d") }}</p>
    <br>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <table class="table">
                        <tr>
                            <th width="25%">Name</th>
                            <td>{{ $data->member->name }}</td>
                        </tr>
                        <tr>
                            <th width="25%">Mobile</th>
                            <td>{{ $data->member->mobile }}</td>
                        </tr>
                        <tr>
                            <th width="25%">Payment Date</th>
                            <td>{{ $data->date }}</td>
                        </tr>
                    </table>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="10%">Sl No.</th>
                                <th width="25%">Payment Type</th>
                                <th width="40%">Details</th>
                                <th width="20%">Amount (BDT)</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1.</td>
                                <td>{{ $data->paymentType->title }}</td>
                                <td>{{ $data->content }}</td>
                                <td>{{ number_format($data->amount, 2) }}</td>
                                <td></td>
                            </tr>
                        </tbody>

                        <tfoot style="border-top: 1px solid #000">
                            <tr>
                                <td colspan="3">Total Amount</td>
                                <td>{{ number_format($data->amount, 2) }} BDT</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


