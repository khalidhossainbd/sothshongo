@extends('layouts.dashlayout')

@section('content')


<div class="container-fluid">
    <h3 class="page-title">EC Members List</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    @if(Session::has('flash_message'))
					    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
					@endif
                </div>
                <div class="panel-body">
                    <div class="row">
                    	<table class="table table-bordered">
                    		<thead>
                    			<tr>
                    				<th>Sl No.</th>
                    				<th>Name</th>
                    				<th>Email</th>
                    				<th>Mobile</th>
                    				<th>Status</th>
                    				<th>Action</th>
                    			</tr>
                    		</thead>
                    		<tbody>
                    			@foreach($mambers as $item)
                    			<tr>
                    				<td>{{ $loop->iteration }}</td>
                    				<td>{{ $item->name }}</td>
                    				<td>{{ $item->email }}</td>
                    				<td>{{ $item->mobile }}</td>
                    				<td>
                    					@php 
                    					    if($item->status >0){
                    					        echo "<p style='color:green'>Active</p>";
                    					    }else{
                    					        echo "<p style='color:red'>Inactive</p>";
                    					    }
                    					@endphp
                    				</td>
                    				<td>
                    					<a href="{{ url('/kadmin/members/' . $item->id) }}" title="View Poem"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                    					<a href="{{ url('/kadmin/members/' . $item->id . '/edit') }}" title="Edit Poem"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                    					<form method="POST" action="{{ url('/kadmin/members' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                    					    {{ method_field('DELETE') }}
                    					    {{ csrf_field() }}
                    					    <button type="submit" class="btn btn-danger btn-sm" title="Delete Poem" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                    					</form>
                    				</td>
                    			</tr>
                    			@endforeach
                    		</tbody>
                    	</table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

