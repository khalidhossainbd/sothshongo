@extends('layouts.dashlayout')

@section('content')
    
<div class="container-fluid">
    <h3 class="page-title">Add New Member</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ url('/kadmin/members') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Member name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Full Name" required="required">
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" required="required">
                            </div>
                            <div class="form-group">
                                <label for="mobile">Mobile Number</label>
                                <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" required="required">
                            </div>
                            <div class="form-group">
                                <label for="mobile">Committee Position (Null Allow)</label>
                                <input type="text" class="form-control" id="mobile" name="position" placeholder="Position">
                            </div>
                            <div class="form-group">
                                <label for="date">Registration Date</label>
                                <input type="date" class="form-control" id="date" name="reg_date" placeholder="Date">
                            </div>
                            <div class="form-group">
                                <label for="name">Member Status</label>
                                <select class="form-control" name="status" required="required">
                                    <option value="">Select an Option</option>
                                    <option value="0">Inactive Member</option>
                                    <option value="1">General Member</option>
                                    <option value="2">EC Memebr</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">Profile</label>
                                <textarea class="form-control" rows="5" name="content"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Save</button>
                                <button type="reset" class="btn btn-info">Reset</button>
                            </div>
                            
                        </div>
                        <div class="col-md-6" style="padding: 0 50px;">
                            <div class="form-group">
                                <label for="image">Member Image</label>
                                <input type="file" id="image" name="mem_image" onchange="readURL(this);">
                                <p class="help-block">Image size: 300 X 400 PX</p>
                                <img id="blah" style="max-height: 250px;" />
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    
@endsection

@section('java_script')
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection