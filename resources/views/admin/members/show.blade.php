@extends('layouts.dashlayout')

@section('content')

<div class="row">

    <div class="col-md-12">
        <div class="panel">
        	<div class="panel-header">
        		<h3 class="page-title">Member Info Details</h3>
        	</div>
            <div class="panel-body">

                <a href="{{ url('/kadmin/members') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <a href="{{ url('/kadmin/members/' . $member->id . '/edit') }}" title="Edit Poem"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                <form method="POST" action="{{ url('kadmin/members' . '/' . $member->id) }}" accept-charset="UTF-8" style="display:inline">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Poem" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                </form>
                <br/>
                <br/>
                <div class="row">
                    <div class="col-md-6">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    
                                    <tr><th>Name </th>
                                        <td> {{ $member->name }} </td></tr>
                                    <tr><th> Email </th>
                                        <td> {{ $member->email }} </td></tr>
                                    <tr><th> Mobile Number </th>
                                        <td> {{ $member->mobile }} </td></tr>
                                    <tr><th> Committee Position </th>
                                        <td> {{ $member->position }} </td></tr>
                                    <tr><th> Registration Date </th>
                                        <td> {{ $member->reg_date }} </td></tr>
                                    <tr><th> Short Note </th>
                                        <td> {!! $member->content !!} </td></tr>
                                    <tr>
                                    	<th> Status </th>
                                        <td> 
                                        	@if($member->status ==0)
												Inactive Member
                                        	@elseif($member->status ==1)
												General Member
                                        	@elseif($member->status ==2)
												EC Memebr
                                        	@endif


                                        </td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <tr><th>Member Image </th>
                            <td>
                            	<img class="img-responsive" src="{{ asset('uploads/members/'.$member->mem_image) }}" alt="img" style="max-height: 200px;"> 
                            </td>
                        </tr>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection