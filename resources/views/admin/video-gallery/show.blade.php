@extends('layouts.dashlayout')

@section('content')

    <div class="container-fluid">
        <!-- OVERVIEW -->
        <div class="panel panel-headline">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="panel-title">Video Gallery Details</h3>
                        <a href="{{ url('/kadmin/video-gallery') }}" class="btn btn-success btn-sm" title="Add New Branch">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                    <div class="col-md-6">

                        
                    </div>
                </div>
                
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        {{-- <tr>
                                            <th>Sl No.</th>
                                            <td></td>
                                        </tr> --}}
                                        <tr>
                                            <th>Gallery Title</th>
                                            <td> {{ $videos->title }} </td>
                                        </tr>
                                        <tr>
                                            <th>Video Url</th>
                                            <td> {{ $videos->video_url }} </td>
                                        </tr>  
                                        <tr>
                                            <th>Description</th>
                                            <td> {{ $videos->content }} </td>
                                        </tr>  
                                        <tr>
                                            <th>Serial Date</th>
                                            <td> {{ $videos->date }} </td>
                                        </tr>  
                                        <tr>
                                            <th>Status</th>
                                            <td>
                                                @php 
                                                    if($videos->status >0){
                                                        echo "Publish";
                                                    }else{
                                                        echo"Unpublish";
                                                    }
                                                @endphp
                                            </td>
                                        </tr>
                                        <tr> 
                                            <th>Image</th>
                                            <td>
                                                <img src="{{ asset('uploads/videoimg/'.$videos->video_img) }}" class="img-thumbnail" style="max-height: 300px;">
                                            </td>
                                        </tr>
                                        <tbody>
                                            {{-- <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->title }}</td>
                                                <td>{{ $item->video_url }}</td>
                                                <td>{{ $item->content }}</td>
                                                <td>{{ $item->date }}</td>
                                                <td>
                                                    @php 
                                                        if($item->status >0){
                                                            echo "Publish";
                                                        }else{
                                                            echo"Unpublish";
                                                        }
                                                    @endphp
                                                </td>
                                                <td>
                                                    <a href="{{ url('/kadmin/video-gallery/' . $item->id) }}" title="View Images"><button class="btn btn-info btn-sm">View </button></a>
                                                    <a href="{{ url('/kadmin/video-gallery/' . $item->id . '/edit') }}" title="Edit Branch"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                                    <form method="POST" action="{{ url('/kadmin/video-gallery' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-danger btn-sm" title="Delete Branch" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                                    </form>
                                                </td>
                                            </tr> --}}
                                        </tbody>
                                    </table>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END OVERVIEW -->
    </div>
        
@endsection
