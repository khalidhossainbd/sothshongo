@extends('layouts.dashlayout')

@section('title', 'Registation form')

@section('content')
	<div class="col-md-9">
	    <div class="panel panel-default">
	        <div class="panel-heading">User List</div>
	        <div class="panel-body">
	            <a href="{{ url('kadmin/register') }}" class="btn btn-success btn-sm" title="Add New Block">
	                <i class="fa fa-plus" aria-hidden="true"></i> Add New
	            </a>
	            <a href="{{ url('/kadmin/dashboard') }}" class="btn btn-success btn-sm" title="Add New Block">
	                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Back
	            </a>

	            <div class="row">
	            	<div class="col-md-6 col-sm-12">
	            		@if(Session::has('flash_message'))
	            		    <div class="alert alert-info">
	            		        <h4>{{ Session::get('flash_message') }}</h4>
	            		        {{ session()->forget('flash_message')}}
	            		    </div>
	            		@endif
	            	</div>
	            	<div class="col-md-6 col-sm-12">
	            		{!! Form::open(['method' => 'GET', 'url' => '/admin/blocks', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
	            		<div class="input-group">
	            		    <input type="text" class="form-control" name="search" placeholder="Search...">
	            		    <span class="input-group-btn">
	            		        <button class="btn btn-success" type="submit">
	            		            <i class="fa fa-search"></i>Search
	            		        </button>
	            		    </span>
	            		</div>
	            		{!! Form::close() !!} 
	            	</div>
	            </div>
	           
	            <div class="table-responsive">
	                <table class="table table-bordered table-hover">
	                    <thead>
	                        <tr>
	                            <th>Sl No.</th>
	                            <th>User Name</th>
								<th>User Email</th>
								<th>User Role</th>
								<th>Action</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    <?php $i= 1; ?>
	                    @foreach($user as $item)
	                        <tr>
	                        	<td> <?php echo $i ;?></td>  
	                            <?php $i++; ?>
	                            <td>{{ $item->name }}</td>
	                            <td>{{ $item->email }}</td>
	                            <td>{{ $item->role }}</td>
	                            <td>
	                            	{!! Form::open([
	                                    'method'=>'DELETE',
	                                    'url' => ['/kadmin/userdelete', $item->id],
	                                    'style' => 'display:inline'
	                                ]) !!}
	                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
	                                            'type' => 'submit',
	                                            'class' => 'btn btn-danger btn-xs',
	                                            'title' => 'Delete Block',
	                                            'onclick'=>'return confirm("Confirm delete?")'
	                                    )) !!}
	                                {!! Form::close() !!}
	                                {{-- <a href="{{ url('/admin/blocks/' . $item->id) }}" title="View Block"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a> --}}
	                               {{--  <a href="{{ url('/admin/blocks/' . $item->id . '/edit') }}" title="Edit Block"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
	                                 --}}
	                            </td>
	                        </tr>
	                    @endforeach
	                    </tbody>
	                </table>
		              <div class="text-center">
		              	{!! $user->links(); !!}
		              </div>
	            </div>

	        </div>
	    </div>
	</div>
@endsection