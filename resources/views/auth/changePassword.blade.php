@extends('layouts.dashlayout')

@section('title', 'Registation form')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-6 col-md-offset-1">
        <div class="card">
            <div class="card-header">{{-- {{ __('Register') }} --}}<h2><strong> Change user password</strong></h2> 
			<hr>
            </div>

            @if(session()->has('flash_message'))
                <ul class="alert alert-success">
                    <li>{{ session()->get('flash_message') }}</li>
                </ul>
            @endif

            <div class="card-body">
                {!! Form::open(['url' => '/kadmin/changePassword', 'method' => 'Patch','role' => 'form', 'files' => true]) !!}

                    <div class="form-group row">
					    <label></label>
					     <label for="password" class="col-md-4 col-form-label text-md-right">User Email:</label>
					    <div class="col-md-6">
					    <input class="form-control" name="email" value="{{ Auth::user()->email }}" disabled>
						</div>
					</div>
                    

                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">Old Password</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="old_password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">New Password</label>

                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" name="new_password" required>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                Change Password
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection