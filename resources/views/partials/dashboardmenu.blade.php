
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="brand" style="padding: 20px 29px;">
      {{-- <a href=""><img src="{{ asset('assets/img/logo_1.png') }}" alt="Klorofil Logo" class="img-responsive logo" style="max-height: 50px;"></a> --}} <p style="text-align: center; font-size: 22px;">SothShongo Shomity</p>
    </div>
    <div class="container-fluid">
      <div class="navbar-btn">
        <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
      </div>
      {{-- <form class="navbar-form navbar-left">
        <div class="input-group">
          <input type="text" value="" class="form-control" placeholder="Search dashboard...">
          <span class="input-group-btn"><button type="button" class="btn btn-primary">Go</button></span>
        </div>
      </form> --}}
      <div id="navbar-menu">
        <ul class="nav navbar-nav navbar-right">
         
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{ asset('assets/img/user.png') }}" class="img-circle" alt="Avatar"> <span>{{ Auth::user()->name }}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
            <ul class="dropdown-menu">
              {{-- <li><a href="#"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>
              <li><a href="#"><i class="lnr lnr-envelope"></i> <span>Message</span></a></li> --}}
              <li><a href="{{ url('/kadmin/changePassword') }}"><i class="lnr lnr-cog"></i> <span>Change Password</span></a></li>
              <li><a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                         <i class="lnr lnr-exit"></i>{{ __('Logout') }}

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
              </a></li>
              {{-- <li><a href="#"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li> --}}
            </ul>
          </li>
          
        </ul>
      </div>
    </div>
  </nav>
  <div id="sidebar-nav" class="sidebar">
    <div class="sidebar-scroll">
      <nav>
        <ul class="nav">
          <li><a href="{{ url('/kadmin/dashboard') }}" class="active"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
          @if(Auth::user()->role == "Supper Admin" )
          <li>
            <a href="#subPages-01" data-toggle="collapse" class="collapsed"><i class="lnr lnr-cog"></i> <span>User Control</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
            <div id="subPages-01" class="collapse ">
              <ul class="nav">
                <li><a href="{{ url('/kadmin/userList') }}" class="">User List</a></li>
                <li><a href="" class="">Login History</a></li>
                <li><a href="" class="">Inactive User List</a></li>
              </ul>
            </div>
          </li>
          @endif

          @if(Auth::user()->role == "Supper Admin" )
          <li>
            <a href="#subPages-02" data-toggle="collapse" class="collapsed"><i class="lnr lnr-user"></i> <span>Member Setting</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
            <div id="subPages-02" class="collapse ">
              <ul class="nav">
                <li><a href="{{ url('/kadmin/members') }}" class="">Member List</a></li>
                <li><a href="{{ url('/kadmin/members/create') }}" class="">Add New Member</a></li>
                <li><a href="{{ url('/kadmin/members/ec-member') }}" class="">EC Member</a></li>
                <li><a href="{{ url('/kadmin/members/inactive') }}" class="">Inactive Member</a></li>
              </ul>
            </div>
          </li>
          @endif
          {{-- <li>
            <a href="#subPages-033" data-toggle="collapse" class="collapsed"><i class="lnr lnr-chart-bars"></i> <span>New applicants</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
            <div id="subPages-033" class="collapse ">
              <ul class="nav">
                <li><a href="{{ url('/kadmin/applications') }}" class="">New Applicants List </a></li>
                <li><a href="{{ url('/kadmin/applications/approvedlist') }}" class="">Approved Applicants</a></li>
                <li><a href="{{ url('/kadmin/applications/rejectedlist') }}" class="">Rejected Applicants</a></li>
              </ul>
            </div>
          </li> --}}

          <li>
            <a href="#subPages-03" data-toggle="collapse" class="collapsed"><i class="lnr lnr-cart"></i> <span>Expenditure</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
            <div id="subPages-03" class="collapse ">
              <ul class="nav">
                <li><a href="" class="">Expenditure List </a></li>
                <li><a href="" class="">Add New Expenditure</a></li>
                <li><a href="" class="">Report</a></li>
              </ul>
            </div>
          </li>
          <li>
            <a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-dice"></i> <span></span>Subscription <i class="icon-submenu lnr lnr-chevron-left"></i></a>
            <div id="subPages" class="collapse ">
              <ul class="nav">
                <li><a href="{{ url('/kadmin/paymenttypes') }}" class="">Subscription Type</a></li>
                {{-- <li><a href="" class="">All Subscription List</a></li> --}}
                <li><a href="{{ route('payment.create') }}" class="">Add Monthly Subscription</a></li>
                <li><a href="{{ route('payment.index') }}" class="">Monthly Subscription</a></li>
                <li><a href="" class="">Report</a></li>
              </ul>
            </div>
          </li>
          @if(Auth::user()->role == "Supper Admin" )
          <li>
            <a href="#subPages001" data-toggle="collapse" class="collapsed"><i class="lnr lnr-dice"></i> <span></span>Meeting Minutes<i class="icon-submenu lnr lnr-chevron-left"></i></a>
            <div id="subPages001" class="collapse ">
              <ul class="nav">
                <li><a href="{{ url('/kadmin/meeting-minutes') }}" class="">Meeting Minutes List</a></li>
                <li><a href="{{ url('/kadmin/meeting-minutes/create') }}" class="">Add Meeting Minutes</a></li>
                <li><a href="" class="">Report</a></li>
              </ul>
            </div>
          </li>
          @else
          <li><a href="" class=""><i class="lnr lnr-alarm"></i> <span>Meeting Minutes</span></a></li>
          
          @endif
          {{-- <li><a href="elements.html" class=""><i class="lnr lnr-code"></i> <span>Elements</span></a></li>
          <li><a href="charts.html" class=""><i class="lnr lnr-chart-bars"></i> <span>Charts</span></a></li>
          
          <li><a href="notifications.html" class=""><i class="lnr lnr-alarm"></i> <span>Notifications</span></a></li> --}}
          <li>
            <a href="#subPages12" data-toggle="collapse" class="collapsed"><i class="lnr lnr-dice"></i> <span>Gallery</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
            <div id="subPages12" class="collapse ">
              <ul class="nav">
                <li><a href="{{ url('/kadmin/photo-gallery') }}" class="">Image Gallery</a></li>
                <li><a href="{{ url('/kadmin/gallery-images/create') }}" class="">Add Images</a></li>
                <li><a href="{{ url('/kadmin/video-gallery') }}" class="">Video Gallery</a></li>
              </ul>
            </div>
          </li>
          <li><a href="" class=""><i class="lnr lnr-file-empty"></i> <span>Document Managment</span></a></li>
          {{-- <li><a href="typography.html" class=""><i class="lnr lnr-text-format"></i> <span>Typography</span></a></li>
          <li><a href="icons.html" class=""><i class="lnr lnr-linearicons"></i> <span>Icons</span></a></li> --}}
          
        </ul>
      </nav>
    </div>
  </div>
    