<section>
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="footer-div">
							<ul class="footer-list">
								<li><strong>About Us</strong></li>
								<hr class="style-one">
								<li><a href="./vission_mission.php">Vision & Mission</a></li>
								<hr class="style-two">
								<li><a href="./our_principles.php">Our Principles</a></li>
								<hr class="style-two">
								<li><a href="./membership.php">Membership</a></li>
								<hr class="style-two">
								<li><a href="#">Policies</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-4">
						<div class="footer-div">
							<ul class="footer-list">
								<li><strong>Services</strong></li>
								<hr class="style-one">
								<li><a href="./transport_fleet_management.php">Transport fleet management</a></li>
								<hr class="style-two">
								<li><a href="./shondhi_e_commerce.php">E-commerce</a></li>
								<hr class="style-two">
								<li><a href="./organic_food.php">Organic Food</a></li>
								<hr class="style-two">
								<li><a href="#">Up Coming...</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-4">
						<div class="footer-div">
							<ul class="footer-list">
								<li><strong>Address:</strong></li>
								<hr class="style-one">
								<p class="add-text" style="color:#fff; padding: 0 30px; ">House# 241, Block# B, <br> Ground Floor, <br>
									Eastern Housing,<br> Pallabi (2nd Phase),<br> Dhaka-1212<br>
									Email: info@honestharvest.org

								</p>
								<!-- <li><a href="#">Footer List</a></li>
								<hr class="style-two">
								<li><a href="#">Footer List</a></li>
								<hr class="style-two">
								<li><a href="#">Footer List</a></li>
								<hr class="style-two">
								<li><a href="#">Footer List</a></li> -->
							</ul>
						</div>
					</div>
				</div>
				<hr class="style-two">
				<div class="row">
					<div class="col-md-6">
						<p class="dev-class">Design & Developed by: Md Khalid Hossain</p>
					</div>
					<div class="col-md-6">
						
					</div>
				</div>
			</div>
		</div>
	</section>