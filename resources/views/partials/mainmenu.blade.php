<!--    <div class="wrapper">
        <header>
            <nav>
               <div class="menu-icon">
                  <i class="fa fa-bars fa-2x"></i>
               </div>
               <div class="logo">
                  <img class="img-responsive" src="dist/images/logo_1.png">
               </div>
               <div class="menu">
                  <ul>
                     <a href="#"><li>Home</li></a>
                     <a href="#"><li>About Us</li></a>
                     <a href="#"><li>Events</li></a>
                     <a href="#"><li>Gallery</li></a>
                     <a href="#"><li>Blog</li></a>
                     <a href="#"><li>Contact Us</li></a>
                  </ul>
               </div>
            </nav>
        </header>
   </div> -->


  <nav class="navbar navbar-default">
     <div class="container">
       <div class="navbar-header">
         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
           <span class="sr-only">Toggle navigation</span>
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
         </button>
         <a class="navbar-brand" href="{{ url('/') }}">
            <img class="img-responsive" src="{{ asset('dist/images/logo-3.png') }}" style="max-height: 60px; padding:3px;">
         </a>
       </div>
       <div id="navbar" class="navbar-collapse collapse">
         <ul class="nav navbar-nav">
           
         </ul>
         <ul class="nav navbar-nav navbar-right">
           <li class="{{ Request::path() == '/' ? 'active' : '' }}" style="border-left: 2px solid #00a0e3;"><a href="{{ url('/') }}">Home</a></li>
           <li class="dropdown {{ Request::path() == '/about_us/' ? 'active' : '' }}" >
             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About Us <span class="caret"></span></a>
             <ul class="dropdown-menu">
              <li><a href="{{ url('about_us/vission_&_mission') }}">Vision & Mission</a></li>
               <li role="separator" class="divider"></li>
               <li><a href="{{ url('about_us/our_principles') }}">Our Principles</a></li>
               <li role="separator" class="divider"></li>
               <li><a href="{{ url('about_us/founder_members') }}">Founder Members</a></li>
               <li role="separator" class="divider"></li>
               <li><a href="{{ url('about_us/executive_members') }}">Executive Members</a></li>
               <li role="separator" class="divider"></li>
               <li><a href="{{ url('about_us/general_members') }}">General Members</a></li>
               {{-- <li role="separator" class="divider"></li>
               <li><a href="#">Policies</a></li> --}}
               {{-- <li role="separator" class="divider"></li> --}}
               {{-- <li {{ Request::path() == 'contact_us' ? 'active' : '' }}><a href="{{ url('/about_us/member_register') }}">Membership</a></li> --}}
             </ul>
           </li>
           <!-- <li><a href="#">About Us</a></li> -->
           <li><a href="#">Events</a></li>
           <li class="dropdown">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gallery <span class="caret"></span></a>
             <ul class="dropdown-menu">
               <li><a href="{{ url('/image_gallery') }}">Image Gallery</a></li>
               <li role="separator" class="divider"></li>
               <li><a href="{{ url('/video_gallery') }}">Video Gallery</a></li>
               
             </ul>
           </li>
           {{-- <li class="dropdown">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <span class="caret"></span></a>
             <ul class="dropdown-menu">
               <li><a href="{{ url('/services/Transport_fleet_management') }}">Transport fleet management</a></li>
               <li role="separator" class="divider"></li>
               <li><a href="{{ url('/services/Shondhi_Bazar') }}">E-commerce</a></li>
               <li role="separator" class="divider"></li>
               <li><a href="{{ url('/services/Organic_Food') }}">Organic Food</a></li>
             </ul>
           </li> --}}
           {{-- <li><a href="#">Services</a></li> --}}
           
           <li><a href="{{ url('/shop') }}">Shop</a></li>
           <li><a href="{{ url('/blog') }}">Blog</a></li>
           <li class="{{ Request::path() == 'contact_us' ? 'active' : '' }}"><a href="{{ url('/contact_us') }}">Contact Us</a></li>
         </ul>
       </div><!--/.nav-collapse -->
     </div>
   </nav>