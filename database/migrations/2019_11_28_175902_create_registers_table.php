<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('f_name')->nullable();
            $table->string('m_name')->nullable();
            $table->string('email')->nullable();
            $table->string('mobile')->nullable();
            $table->date('date')->nullable();
            $table->string('occupation')->nullable();
            $table->string('nid')->nullable();
            $table->text('address')->nullable();
            $table->string('n_name')->nullable();
            $table->string('n_relation')->nullable();
            $table->string('nationality')->nullable();
            $table->string('myimage')->nullable();
            $table->string('conditions')->nullable();
            $table->integer('status')->default(0);
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registers');
    }
}
