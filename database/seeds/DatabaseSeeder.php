<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert([
            'name' => 'Khalid Hossain',
            'email' => 'kadmin@gmail.com',
            'role'=> 'Supper Admin',
            'password' => bcrypt('12345678'),
        ]);
    }
}
