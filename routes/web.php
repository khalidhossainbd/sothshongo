<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This application Design and develop by Md Khalid Hossain
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('config/cache',function(){
    $exitCode = Artisan::call('config:cache');
}); 

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});

Route::get('logout',function(){
    return view('errors.404');
});

Route::get('/', 'MainController@index');
Route::get('/about_us/vission_&_mission', 'MainController@vission');
Route::get('/about_us/our_principles', 'MainController@principles');
Route::get('/about_us/founder_members', 'MainController@founders');
Route::get('/about_us/executive_members', 'MainController@executive');
Route::get('/about_us/general_members', 'MainController@general');
Route::get('/contact_us', 'MainController@contact');
Route::get('/about_us/member_register', 'MainController@register');
Route::post('/member_register', 'MainController@saveregister');

Route::get('/shop', 'MainController@shop');
Route::get('/blog', 'MainController@blog');

Route::get('/services/Transport_fleet_management', 'MainController@fleet_management');
Route::get('/services/Shondhi_Bazar', 'MainController@Shondhi_Bazar');
Route::get('/services/Organic_Food', 'MainController@Organic_Food');

Route::get('/image_gallery', 'MainController@imagegallery');
Route::get('/video_gallery', 'MainController@videogallery');

Route::get('kadmin/members/inactive', 'Admin\\MemberController@inactive');
Route::get('kadmin/members/ec-member', 'Admin\\MemberController@ecmember');

Route::group(['prefix'=>'kadmin'], function(){
    Auth::routes();
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
    Route::get('/userList', 'UserController@Index')->name('userlist');
    Route::post('/createUser', 'UserController@storeuser')->name('storeuser');
    Route::get('/changePassword', 'UserController@EditUser')->name('EditUser');
    Route::patch('/changePassword', 'UserController@Update');
    Route::delete('/userdelete/{id}', 'UserController@Destory');
    Route::get('/applications/approvedlist', 'Admin\\RegisterController@approvedlist');
    Route::get('/applications/rejectedlist', 'Admin\\RegisterController@rejectedlist');
    Route::get('/applications/pdfdownload/{id}', 'Admin\\RegisterController@pdfdownload');

    Route::get('/payment/print/{id}', 'Admin\\PrintController@prnpriview');

    Route::resource('/poems', 'Admin\\PoemsController');
    Route::resource('/members', 'Admin\\MemberController');
    Route::resource('/applications', 'Admin\\RegisterController');
    Route::resource('/paymenttypes', 'Admin\\PaymentTypeController');
    Route::resource('/payment', 'Admin\\PaymentController');
    Route::resource('/meeting-minutes', 'Admin\\MeetingController');
});

Route::resource('kadmin/photo-gallery', 'Admin\\PhotoGalleryController');
Route::resource('kadmin/gallery-images', 'Admin\\GalleryImageController');
Route::resource('kadmin/video-gallery', 'Admin\\VideoGalleryController');

Route::get('/{any}', function ($any) {
  return Redirect::to('/');
})->where('any', '.*');
