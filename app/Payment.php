<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payments';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['amount', 'date', 'status', 'content', 'user_id', 'payment_type_id', 'member_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function paymentType()
    {
        return $this->belongsTo('App\PaymentType', 'payment_type_id');
    }

    public function member()
    {
        return $this->belongsTo('App\Member', 'member_id');
    }


}
