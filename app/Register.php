<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'registers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'f_name', 'm_name', 'email', 'mobile', 'date', 'occupation', 'nid', 'address', 'n_name', 'n_relation', 'nationality', 'myimage', 'conditions', 'status', 'note'];
}
