<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poem extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'poems';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'author', 'publishDate', 'poemImg', 'contentBangla', 'contentEnglist'];

    
}
