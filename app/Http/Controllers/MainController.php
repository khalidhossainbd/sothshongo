<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Member;
use App\PhotoGallery;
use App\GalleryImage;
use App\VideoGallery;

use App\Register;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

use File;
use Image;
use Session;
use Auth;

class MainController extends Controller
{
    public function index()
    {
    	return view('pages.index');
    }

    public function fleet_management()
    {
        return view('pages.services.fleet_management');
    }
    public function Shondhi_Bazar()
    {
        return view('pages.services.bazar');
    }
    public function Organic_Food()
    {
        return view('pages.services.organic');
    }

    public function vission()
    {
    	return view('pages.about.vission');
    }
    public function principles()
    {
    	return view('pages.about.principles');
    }
    public function founders()
    {
    	return view('pages.about.founders');
    }
    public function executive()
    {
        $mambers = Member::where('status', '=', '2')->get();
    	return view('pages.about.executive', compact('mambers'));
    }

    public function general()
    {
        $mambers = Member::where('status', '>', '0')->get();
        // dd($mambers);
    	return view('pages.about.general', compact('mambers'));
    }

    public function register()
    {
    	return view('pages.about.register');
    }

    public function saveregister(Request $request)
    {
    	// dd($request);
        $request->validate([
            'name' => 'required',
            'mobile' => 'required',
            'nid' => 'required',
            'email' => 'required|unique:registers|email',
        ]);


        $file = Input::file('myimage');

        if (!empty($file)){

            $image = Image::make($file);

            $path = 'uploads/members/new/';

            $fileName = $request->file('myimage')->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).".".$filetype;
          
            //$image->save($path.$filenewname);
            
            $image->resize(300, 400);
            $image->save($path.$filenewname);

            $mypoem = new Register();
            $mypoem->name = $request->name;
            $mypoem->f_name = $request->f_name;
            $mypoem->m_name = $request->m_name;
            $mypoem->email = $request->email;
            $mypoem->mobile = $request->mobile;
            $mypoem->date = $request->date;
            $mypoem->occupation = $request->occupation;
            $mypoem->nid = $request->nid;
            $mypoem->address = $request->address;
            $mypoem->n_name = $request->n_name;
            $mypoem->n_relation = $request->n_relation;
            $mypoem->nationality = $request->nationality;
            $mypoem->conditions = $request->conditions;
            $mypoem->myimage = $filenewname;
            $mypoem->save();

        }else{
            $requestData = $request->all();
            Register::create($requestData);
        }

        return redirect('/about_us/member_register')->with('message', 'Thanks For Register. We will inform you soon!');

    }

    public function contact()
    {
        return view('pages.contact');
    }
    public function imagegallery()
    {
        $gallery = PhotoGallery::where('status', '=', '1')->get();
        // dd($gallery);
        return view('pages.gallery.imggallery', compact('gallery'));
    }
    public function videogallery()
    {
        $video = VideoGallery::where('status', '=', '1')->get();
        return view('pages.gallery.videogallery', compact('video'));
    }

    public function blog()
    {
        return view('pages.products.blog');
    }

    public function shop()
    {
        return view('pages.products.shop');
    }
}
