<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Session;
use Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    public function Index()
    {
        if(Auth::user()->role == "Supper Admin"){
            $user = User::orderBy('id', 'desc')->paginate(10);
            return view('auth.userlist', compact('user'));
            
        }else{
            return redirect('/kadmin/dashboard');
        }
        
    }
    public function storeuser(Request $request){
        // dd($request);
        if(Auth::user()->role != "Supper Admin"){
            return redirect('/kadmin/dashboard');
        }
        
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'role' => 'required|string',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);

        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'role' => $request['role'],
            'password' => Hash::make($request['password']),
        ]);

        return redirect('/kadmin/userList');
    }
    public function EditUser(){
    	return view('auth.changePassword');
    }

    public function Update(Request $request){
    	// dd($request);
    	$this->validate($request, [
            'old_password' => 'required',
            'new_password' => 'required|string|min:8',            
        ]);
    	if(Hash::check($request->old_password, Auth::user()->password)){
    		$user=User::find(Auth::user()->id);
    		$user->email=Auth::user()->email;
    		$user->password=bcrypt($request->new_password);
    		// dd($user->password);
    		$user->update();
            session()->flash('flash_message', 'User infomation is Updated!');
            return view('auth.changePassword');
    	}else{
    		return back()->withErrors(['old_password'=>"Old Password didn't match"]);
    	}
    	
    }


    public function Destory($id){
    	if((Auth::user()->role == "Supper Admin") && (Auth::user()->id != $id)){
    		User::destroy($id);
    		Session::flash('flash_message', 'User Deleted successfuly!');
    		return $this->Index();
    	}else{
    		Session::flash('flash_message', 'Sorry not authorized to delete!');
    		return $this->Index();
    	}
    }
}
