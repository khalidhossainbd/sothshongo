<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Register;
use PDF;

class RegisterController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $perPage = 25;
        $applicants = Register::where('status', '<', '2')->paginate($perPage);
        return view('admin.applicants.index', compact('applicants'));
    }

    public function show($id)
    {
    	$applicant = Register::findOrFail($id);
    	return view('admin.applicants.show', compact('applicant'));
    }

    public function edit($id)
    {
    	$applicant = Register::findOrFail($id);
    	return view('admin.applicants.edit', compact('applicant'));
    }

    public function update(Request $request, $id)
    {
    	$requestData = $request->all();
    	$poem = Register::findOrFail($id);
    	$poem->update($requestData);
    	return redirect('kadmin/applications')->with('flash_message', 'Applicant info updated!');
    }

    public function approvedlist()
    {
        $perPage = 25;
        $applicants = Register::where('status', '=', '2')->paginate($perPage);

        return view('admin.applicants.approved', compact('applicants'));
    }
    public function rejectedlist()
    {
        $perPage = 25;
        $applicants = Register::where('status', '=', '3')->paginate($perPage);
        return view('admin.applicants.rejected', compact('applicants'));
    }

    public function pdfdownload($id){
        $applicant = Register::findOrFail($id);
        $pdf = PDF::loadView('admin.applicants.form', compact('applicant'));
        return $pdf->download('form.pdf');
    }
}
