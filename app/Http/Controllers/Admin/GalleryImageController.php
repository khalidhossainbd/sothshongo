<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\GalleryImage;
use App\PhotoGallery;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use File;
use Image;
use Session;
use Auth;

class GalleryImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $galleryimages = GalleryImage::where('title', 'LIKE', "%$keyword%")
                ->orWhere('images', 'LIKE', "%$keyword%")
                ->orWhere('photo_gallery_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $galleryimages = GalleryImage::latest()->paginate($perPage);
        }

        return view('admin.gallery-images.index', compact('galleryimages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $photogallery = PhotoGallery::all();
        return view('admin.gallery-images.create', compact('photogallery'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'title' => 'required',
            'images' => 'required',
            'photogallery_id' => 'required'
        ]);

        $file = Input::file('images');
        
        if (!empty($file))
        {
            $file_len = count($file);
            $path = 'uploads/photogallery/';
            //dd($file_len);
            for($i = 0; $i< $file_len; $i++)
            {
                $image = Image::make($file[$i]);

                $fileName = $file[$i]->getClientOriginalName();

                $extension = explode(".", strtolower($fileName));
                $filetype = end($extension);
                $filenewname = rand(1000000, 999999999).".".$filetype;

                $image->resize(940, 629);
                
                $image->save($path.$filenewname);

                $brimg = new GalleryImage();
                $brimg->title = $request->title;
                $brimg->photo_gallery_id = $request->photogallery_id;
                $brimg->images = $filenewname;
                $brimg->save();
            }

        }else{
            $requestData = $request->all();
            
            GalleryImage::create($requestData);
        }
        
        return redirect('kadmin/gallery-images')->with('flash_message', 'IMAGE ADD!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $mypoem = GalleryImage::find($id);

        if($mypoem['images']){
            $pathToImage = 'uploads/photogallery/'.$mypoem['images'];
            File::delete($pathToImage);
        }

        $mypoem->delete();

        GalleryImage::destroy($id);

        return redirect('kadmin/gallery-images')->with('flash_message', 'Image deleted!');
    }
}
