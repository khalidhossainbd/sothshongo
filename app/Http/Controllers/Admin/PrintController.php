<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Payment;
use App\Member;
use App\PaymentType;
use App\User;

use Session;
use Auth;

class PrintController extends Controller
{
   	public function __construct()
    {
        $this->middleware('auth');
    }

    public function prnpriview($id)
    {
        $data = Payment::findOrFail($id);
        return view('admin.payment.print', compact('data'));
    }
}
