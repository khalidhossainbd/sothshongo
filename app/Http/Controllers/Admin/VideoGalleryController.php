<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\VideoGallery;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use File;
use Image;
use Session;
use Auth;

class VideoGalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $videos = VideoGallery::where('title', 'LIKE', "%$keyword%")
                ->orWhere('video_url', 'LIKE', "%$keyword%")
                ->orWhere('date', 'LIKE', "%$keyword%")
                ->orWhere('video_img', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->orWhere('content', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $videos = VideoGallery::latest()->paginate($perPage);
        }

        return view('admin.video-gallery.index', compact('videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.video-gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'title' => 'required',
            'video_url' => 'required',
            'video_img' => 'required',
            'date' => 'required',
            'status' => 'required'
        ]);

        $file = Input::file('video_img');
        
        if (!empty($file)){

            $image = Image::make($file);

            $path = 'uploads/videoimg/';

            $fileName = $request->file('video_img')->getClientOriginalName();

            //dd($fileName);
            
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).".".$filetype;
          
            //$image->save($path.$filenewname);

            $image->resize(600, 400);
            
            $image->save($path.$filenewname);

            $brimg = new VideoGallery();
            $brimg->title = $request->title;
            $brimg->video_url = $request->video_url;
            $brimg->date = $request->date;
            $brimg->content = $request->content;
            $brimg->status = $request->status;
            $brimg->video_img = $filenewname;
            $brimg->save();

        }else{
            $requestData = $request->all();
            
            VideoGallery::create($requestData);
        }
        return redirect('kadmin/video-gallery')->with('flash_message', 'Video added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $videos = VideoGallery::findOrFail($id);

        return view('admin.video-gallery.show', compact('videos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $videos = VideoGallery::findOrFail($id);

        return view('admin.video-gallery.edit', compact('videos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $request->validate([
            'title' => 'required',
            'video_url' => 'required',
            'date' => 'required',
            'status' => 'required'
        ]);

        $file = Input::file('images');

        if (!empty($file)){

            $myvideo = VideoGallery::findOrFail($id);
            // dd($menu);
            $pathToImage = 'uploads/videoimg/'.$myvideo['video_img'];

            $image = Image::make($file);

            $path = 'uploads/videoimg/';

            $fileName = $request->file('video_img')->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).".".$filetype;
          
            //$image->save($path.$filenewname);
            
            $image->resize(600, 400);
            $image->save($path.$filenewname);

            if($image->save()==true){
                File::delete($pathToImage);
            }

            $myvideo->title = $request->title;
            $myvideo->video_url = $request->video_url;
            $myvideo->date = $request->date;
            $myvideo->content = $request->content;
            $myvideo->status = $request->status;
            $myvideo->video_img = $filenewname;
            $menu->update();

        }else{
        
            $requestData = $request->all();
        
            $menu = VideoGallery::findOrFail($id);
            $menu->update($requestData);
        }
        
        

        return redirect('kadmin/video-gallery')->with('flash_message', 'Video gallery updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mypoem = VideoGallery::find($id);

        if($mypoem['video_img']){
            $pathToImage = 'uploads/videoimg/'.$mypoem['video_img'];
            File::delete($pathToImage);
        }

        $mypoem->delete();

        VideoGallery::destroy($id);

        return redirect('kadmin/video-gallery')->with('flash_message', 'Video Gallery deleted!');
    }
}
