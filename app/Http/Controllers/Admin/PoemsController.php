<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

use App\Poem;
use Illuminate\Http\Request;
use File;
use Image;
use Session;
use Auth;

class PoemsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $poems = Poem::where('title', 'LIKE', "%$keyword%")
                ->orWhere('author', 'LIKE', "%$keyword%")
                ->orWhere('publishDate', 'LIKE', "%$keyword%")
                ->orWhere('poemImg', 'LIKE', "%$keyword%")
                ->orWhere('contentBangla', 'LIKE', "%$keyword%")
                ->orWhere('contentEnglist', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $poems = Poem::latest()->paginate($perPage);
        }

        return view('admin.poems.index', compact('poems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.poems.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'title' => 'required|unique:poems|max:255',
            'author' => 'required',
            'publishDate'=>'required'
        ]);

        $file = Input::file('poemImg');

        if (!empty($file)){

            $image = Image::make($file);

            $path = 'uploads/containImg/';

            $fileName = $request->file('poemImg')->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).".".$filetype;
          
            //$image->save($path.$filenewname);
            
            $image->resize(400, 400);
            $image->save($path.$filenewname);

            $mypoem = new Poem();
            $mypoem->title = $request->title;
            $mypoem->author = $request->author;
            $mypoem->publishDate = $request->publishDate;
            $mypoem->contentBangla = $request->contentBangla;
            $mypoem->contentEnglist = $request->contentEnglist;
            $mypoem->poemImg = $filenewname;
            $mypoem->save();

        }else{
            $requestData = $request->all();
            Poem::create($requestData);
        }

        return redirect('kadmin/poems')->with('flash_message', 'Poem added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $poem = Poem::findOrFail($id);

        return view('admin.poems.show', compact('poem'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $poem = Poem::findOrFail($id);

        return view('admin.poems.edit', compact('poem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|max:255',
            'author' => 'required',
            'publishDate'=>'required'
        ]);

        $file = Input::file('poemImg');

        if (!empty($file)){

            $mypoem = Poem::find($id);
            $pathToImage = 'uploads/containImg/'.$mypoem['poemImg'];

            $image = Image::make($file);

            $path = 'uploads/containImg/';

            $fileName = $request->file('poemImg')->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).".".$filetype;
          
            //$image->save($path.$filenewname);
            
            $image->resize(400, 400);
            $image->save($path.$filenewname);

            if($image->save()==true){
                File::delete($pathToImage);
            }
            
            $mypoem->title = $request->title;
            $mypoem->author = $request->author;
            $mypoem->publishDate = $request->publishDate;
            $mypoem->contentBangla = $request->contentBangla;
            $mypoem->contentEnglist = $request->contentEnglist;
            $mypoem->poemImg = $filenewname;
            $mypoem->update();

        }else{
            $requestData = $request->all();
            $poem = Poem::findOrFail($id);
            $poem->update($requestData);
        }

        return redirect('kadmin/poems')->with('flash_message', 'Poem updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        //Poem::destroy($id);

        $mypoem = Poem::find($id);

        if($mypoem['poemImg']){
            $pathToImage = 'upload/committee/'.$mypoem['poemImg'];

            File::delete($pathToImage);
        }
        $mypoem->delete();

        return redirect('kadmin/poems')->with('flash_message', 'Poem deleted!');
    }
}
