<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

use App\Member;

use File;
use Image;
use Session;
use Auth;

class MemberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::user()->role != "Supper Admin"){
            return redirect('/kadmin/dashboard');
        }
        // $keyword = $request->get('search');
        // $perPage = 25;

        // if (!empty($keyword)) {
        //     $mambers = Member::where('name', 'LIKE', "%$keyword%")
        //         ->orWhere('mem_image', 'LIKE', "%$keyword%")
        //         ->orWhere('reg_date', 'LIKE', "%$keyword%")
        //         ->orWhere('status', 'LIKE', "%$keyword%")
        //         ->orWhere('content', 'LIKE', "%$keyword%")
        //         ->latest()->paginate($perPage);
        // } else {
        //     $mambers = Member::latest()->paginate($perPage);
        // }
        $mambers = Member::all();

        return view('admin.members.index', compact('mambers'));
        
    }

    public function inactive()
    {
        if(Auth::user()->role != "Supper Admin"){
            return redirect('/kadmin/dashboard');
        }

        $mambers = Member::where('status', '=', '0')->get();

        return view('admin.members.inactive', compact('mambers'));
    }

    public function ecmember()
    {
        if(Auth::user()->type != "Supper Admin"){
            return redirect('/kadmin/dashboard');
        }
        
        $mambers = Member::where('status', '=', '2')->get();

        return view('admin.members.ecmember', compact('mambers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->role != "Supper Admin"){
            return redirect('/kadmin/dashboard');
        }
        return view('admin.members.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->role != "Supper Admin"){
            return redirect('/kadmin/dashboard');
        }

        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'mobile' => 'required',
            'mem_image'=>'required'
        ]);

        $file = Input::file('mem_image');

        if (!empty($file)){

            $image = Image::make($file);

            $path = 'uploads/members/';

            $fileName = $request->file('mem_image')->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).".".$filetype;
          
            //$image->save($path.$filenewname);
            
            $image->resize(600, 700);
            $image->save($path.$filenewname);

            $mypoem = new Member();
            $mypoem->name = $request->name;
            $mypoem->position = $request->position;
            $mypoem->email = $request->email;
            $mypoem->mobile = $request->mobile;
            $mypoem->reg_date = $request->reg_date;
            $mypoem->status = $request->status;
            $mypoem->content = $request->content;
            $mypoem->mem_image = $filenewname;
            $mypoem->save();

        }else{
            $requestData = $request->all();
            Member::create($requestData);
        }

        return redirect('kadmin/members')->with('flash_message', 'Member added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->role != "Supper Admin"){
            return redirect('/kadmin/dashboard');
        }

        $member = Member::findOrFail($id);
        return view('admin.members.show', compact('member'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->role != "Supper Admin"){
            return redirect('/kadmin/dashboard');
        }

        $member = Member::findOrFail($id);
        return view('admin.members.edit', compact('member'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->role != "Supper Admin"){
            return redirect('/kadmin/dashboard');
        }

        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'mobile' => 'required',
            
        ]);

        $file = Input::file('mem_image');

        if (!empty($file)){

            $mypoem = Member::find($id);
            $pathToImage = 'uploads/members/'.$mypoem['mem_image'];

            $image = Image::make($file);

            $path = 'uploads/members/';

            $fileName = $request->file('mem_image')->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).".".$filetype;
          
            //$image->save($path.$filenewname);
            
            $image->resize(600, 700);
            $image->save($path.$filenewname);

            if($image->save()==true){
                File::delete($pathToImage);
            }
            
            $mypoem->name = $request->name;
            $mypoem->position = $request->position;
            $mypoem->email = $request->email;
            $mypoem->mobile = $request->mobile;
            $mypoem->reg_date = $request->reg_date;
            $mypoem->status = $request->status;
            $mypoem->content = $request->content;
            $mypoem->mem_image = $filenewname;
            $mypoem->update();

        }else{
            $requestData = $request->all();
            $poem = Member::findOrFail($id);
            $poem->update($requestData);
        }

        return redirect('kadmin/members')->with('flash_message', 'Member info updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->role != "Supper Admin"){
            return redirect('/kadmin/dashboard');
        }

        $mypoem = Member::find($id);

        if($mypoem['mem_image']){
            $pathToImage = 'upload/members/'.$mypoem['mem_image'];

            File::delete($pathToImage);
        }
        $mypoem->delete();

        return redirect('kadmin/members')->with('flash_message', 'Member deleted!');
    }
}
