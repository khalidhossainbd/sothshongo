<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // if(Auth::guard('kadmin')->guest()){
        //     if($request->ajax() || $request->wantsJson()){
        //         return response('Unauthorized.', 401);
        //     }else{
        //         return redirect()->guest('/kadmin/login');
        //     }
        // }
        // $response = $next($request);
        if (Auth::guard($guard)->check()) {
            return redirect('/kadmin/dashboard');
        }

        return $next($request);
        // return $response->header('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate')
        // ->header('Pragma', 'no-cache')
        // ->header('Expires', 'Sat, 01 Jan 1990 00:00:00 GMt');
    }
}
