<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'members';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'position', 'email', 'mobile', 'mem_image', 'reg_date', 'status', 'content'];

    public function payment()
    {
        return $this->hasMany('App\Payment');
    }

}
