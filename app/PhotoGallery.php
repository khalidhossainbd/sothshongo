<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotoGallery extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'photo_galleries';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'date', 'sl_date', 'content', 'status'];

    public function GalleryImage()
    {
        return $this->hasMany('App\GalleryImage');
    }

}
